// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromMap(jsonString);

import 'dart:convert';

class LoginResponse {
  LoginResponse({
    required this.operacion,
    required this.mensaje,
    required this.token,
    required this.id,
    required this.tipo,
  });

  final bool operacion;
  final String mensaje;
  final String token;
  final String id;
  final String tipo;

  factory LoginResponse.fromJson(String str) =>
      LoginResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory LoginResponse.fromMap(Map<String, dynamic> json) => LoginResponse(
        operacion: json["operacion"],
        mensaje: json["mensaje"],
        token: json["token"] ?? '',
        id: json["id"] ?? '',
        tipo: json["tipo"] ?? '',
      );

  Map<String, dynamic> toMap() => {
        "operacion": operacion,
        "mensaje": mensaje,
        "token": token,
        "id": id,
        "tipo": tipo,
      };
}
