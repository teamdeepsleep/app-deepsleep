import 'package:app_deepsleep/src/controllers/profile_especialista_controller.dart';
// import 'package:app_deepsleep/src/models/especialista_model.dart';
import 'package:app_deepsleep/src/models/response/detalle_especialista_response.dart';
import 'package:app_deepsleep/src/themes/app_themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SpecialistsScreen extends StatelessWidget {
  const SpecialistsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(ProfileEspecialistaController());
    return GetBuilder<ProfileEspecialistaController>(builder: (controller) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Especialistas'),
        ),
        body: FutureBuilder<List<DetalleEspecialista>>(
          future: controller.listarEspecialistas(),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.data == null) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            List<DetalleEspecialista> datos = snapshot.data;
            return Center(
              child: ListView.builder(
                  itemCount: datos.length,
                  itemBuilder: (context, index) {
                    return _item(datos[index], context, controller);
                  }),
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, 'mi_specialit');
          },
          child: const Icon(Icons.person),
        ),
      );
    });
  }
}

Widget _item(DetalleEspecialista elemento, BuildContext context,
    ProfileEspecialistaController controller) {
  return InkWell(
    onTap: () async {
      Navigator.pushNamed(context, 'specialist_detalle', arguments: elemento);
    },
    child: Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      elevation: 15,
      shadowColor: AppTheme.primary.withOpacity(0.4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          FadeInImage(
            image: NetworkImage(elemento.imagen),
            placeholder: const AssetImage('assets/img/loading.gif'),
            width: 75,
            height: 75,
            fit: BoxFit.cover,
          ),
          SizedBox(
            // color: Colors.red,
            height: 75,
            //   width: 169,
            child: Center(
              child: Text('${elemento.nombre} ${elemento.apellido}'),
            ),
          ),
          ElevatedButton(
            child: const Text('seleccionar'),
            onPressed: () async {
              controller.asignarEspecialista(elemento.id);
            },
          ),
        ],
      ),
    ),
  );
}
