import 'package:app_deepsleep/src/environment/index.dart';
import 'package:app_deepsleep/src/models/habito_model.dart';
import 'package:app_deepsleep/src/models/response/habito_response.dart';
import 'package:get/get.dart';

class HabitoProvider extends GetConnect {
  static const apiUrl = Environment.baseUrl;
  @override
  void onInit() {}

  listarMisHabitos(idPciente) async {
    var url = '$apiUrl/habito/paciente/$idPciente';
    final resp = await get(url);
    if (resp.statusCode == 200) {
      final habitosResponse = HabitoResponse.fromMap(resp.body);
      List<Habito> habitos = habitosResponse.data;
      return habitos;
    } else {
      return [];
    }
  }

  guardarMisHabitos(idPciente, data, habitos) async {
    var url = '$apiUrl/habito/paciente/$idPciente';
    final resp = await post(url, {"habitos": habitos});

    return resp.body;
  }
}
