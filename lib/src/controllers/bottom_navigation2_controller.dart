import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class BottomNavigation2Controller extends GetxController {
  var selectIndex = 0;
  var tituloAppBar = 'App';

  BottomNavigation2Controller() {}
  changeIndex(int i) {
    selectIndex = i;
    switch (selectIndex) {
      case 0:
        tituloAppBar = 'Universitarios posibles';
        break;
      case 1:
        tituloAppBar = 'Perfil';
        break;
      case 2:
        tituloAppBar = 'Mis Universitarios';
        break;
      default:
    }
    update();
  }

  logout() {
    var storage = GetStorage();
    storage.remove('isLogued');
    storage.remove('token');
    storage.remove('usuarioTipo');
    storage.remove('idUsuario');
  }
}
