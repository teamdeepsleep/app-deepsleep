import 'package:app_deepsleep/src/controllers/schedule_controller.dart';
import 'package:app_deepsleep/src/models/alarm_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ScheduleScreen extends StatelessWidget {
  const ScheduleScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(ScheduleController());

    return GetBuilder<ScheduleController>(builder: (controller) {
      var lista = controller.listAlarms;
      return Scaffold(
        body: Column(
          children: [
            Container(
                padding: const EdgeInsets.all(15),
                width: double.infinity,
                height: 260,
                child: _header(lista.length, controller, context)),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListView.builder(
                  itemCount: lista.length,
                  itemBuilder: (context, index) {
                    return _cardAlarm(lista[index], index, controller);
                  }),
            ))
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            var resultBack =
                await Navigator.pushNamed(context, 'schedelu_datos');
            if (resultBack != null) {
              controller.agregarAlarma(resultBack);
            }
          },
          child: const Icon(Icons.alarm),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      );
    });
  }
}

Widget _header(int i, ScheduleController controller, BuildContext context) {
  if (i > 0) {
    return Center(
      child: Text(
        'Existen $i alarmas disponibles',
      ),
    );
  } else {
    return const Center(
      child: Text(
        'No existen alarmas',
        style: TextStyle(fontSize: 24),
      ),
    );
  }
}

Widget _cardAlarm(Alarm alarm, int index, ScheduleController controller) {
  return Container(
    //   width: 100,
    height: 100,
    // color: Colors.amber,
    child: Card(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
              width: 250,
              //   color: Colors.amber,
              child: Column(children: [
                Row(
                  children: [
                    Image.asset(
                      'assets/img/luna.png',
                      fit: BoxFit.contain,
                      height: 40,
                      width: 40,
                    ),
                    Text(alarm.alarmaTextoInicial()),
                  ],
                ),
                Row(
                  children: [
                    Image.asset(
                      'assets/img/reading.png',
                      fit: BoxFit.cover,
                      height: 40,
                      width: 40,
                    ),
                    Text(alarm.alarmaTextoFinall()),
                  ],
                )
              ])),
          Container(
              //   color: Colors.red,
              child: Switch(
            value: alarm.isActived,
            onChanged: (value) {
              controller.cambiarEstadoAlarma(value, index);
            },
          ))
        ],
      ),
    ),
  );
}
