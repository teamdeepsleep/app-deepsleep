import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class BottomNavigationController extends GetxController {
  var selectIndex = 0;
  var tituloAppBar = 'App';

  BottomNavigationController();
  changeIndex(int i) {
    selectIndex = i;
    switch (selectIndex) {
      case 0:
        tituloAppBar = 'Recomendaciones';
        break;
      case 1:
        tituloAppBar = 'Mensajes';
        break;
      case 2:
        tituloAppBar = 'Alarma';
        break;
      case 3:
        tituloAppBar = 'Música';
        break;
      case 4:
        tituloAppBar = 'Dashboard';
        break;
      default:
    }
    update();
  }

  logout() {
    var storage = GetStorage();
    storage.remove('isLogued');
    storage.remove('token');
    storage.remove('usuarioTipo');
    storage.remove('idUsuario');
  }
}
