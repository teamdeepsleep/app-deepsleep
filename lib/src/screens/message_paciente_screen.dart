import 'package:app_deepsleep/src/controllers/message_paciente_controller.dart';
import 'package:app_deepsleep/src/themes/app_themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MessagePacienteScreen extends StatelessWidget {
  const MessagePacienteScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(MessagePacienteController());
    return GetBuilder<MessagePacienteController>(builder: (controller) {
      return Scaffold(
        body: Center(
          child: ListView.builder(
              itemCount: 4,
              itemBuilder: (context, index) {
                return _item(index);
              }),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            displayDialogAndroid2(context, controller);
          },
          child: const Icon(Icons.add),
        ),
      );
    });
  }
}

Widget _item(int i) {
  return Dismissible(
    key: UniqueKey(),
    background: Container(color: AppTheme.primary),
    child: Card(
      child: SizedBox(
        height: 80,
        child: ListTile(
          title: Text('mensaje $i'),
        ),
      ),
    ),
  );
}


void displayDialogAndroid2(
    BuildContext context, MessagePacienteController controller) {
  showDialog(
      barrierDismissible: false,
      context: context,
      builder: (_) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadiusDirectional.circular(15)),
          elevation: 5,
          title: const Text('Mensajería'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              //   const Text('Ingrese un nuevo teléfono'),
              const SizedBox(height: 15),
              TextFormField(
                maxLines: 8,
                keyboardType: TextInputType.number,
                controller: controller.mensaje,
                decoration: const InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    border: OutlineInputBorder(),
                    hintText: 'Ingrese un mensaje'),
              ),
            ],
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('Cancelar'),
            ),
            TextButton(
              onPressed: () async {
                controller.enviarMensaje();
                Navigator.pop(context);
              },
              child: const Text('Ok'),
            )
          ],
        );
      });
}
