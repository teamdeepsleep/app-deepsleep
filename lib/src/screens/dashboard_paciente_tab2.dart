import 'package:flutter/material.dart';
import 'package:charts_painter/chart.dart';

class DashboardPacienteTab2Screen extends StatelessWidget {
  const DashboardPacienteTab2Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Stack(
            children: [
              Column(
                children: [
                  _viewDashBoard(context),
                  const SizedBox(height: 15),
                  Container(
                    margin: const EdgeInsets.only(left: 35),
                    width: double.infinity,
                    child: const Text(
                      'Mes a consultar',
                      style:
                          TextStyle(fontWeight: FontWeight.w500, fontSize: 30),
                    ),
                  ),
                  const SizedBox(height: 15),
                  _viewMeses(context),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _viewDashBoard(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10),
      width: double.infinity,
      height: 300.0,
      // color: Colors.yellow,
      child: Chart(
        state: ChartState.bar(
            ChartData.fromList(
              <double>[1, 3, 4, 2, 7, 6, 2, 5, 4, 2, 5, 4, 4]
                  .map((e) => BarValue<void>(e))
                  .toList(),
            ),
            foregroundDecorations: [
              BorderDecoration(borderWidth: 1.0),

              /// ADDED: Add spark line decoration ([SparkLineDecoration]) on foreground
              // SparkLineDecoration(),
            ],
            //   itemOptions: BubbleItemOptions(
            //       // padding: const EdgeInsets.symmetric(horizontal: 4.0),
            //       // maxBarWidth: 10,
            //       ),
            //   behaviour: ChartBehaviour(
            //     // 1) Make sure the chart can scroll
            //     isScrollable: true,
            //   ),
            backgroundDecorations: [
              HorizontalAxisDecoration(
                endWithChart: false,
                lineWidth: 2.0,
                axisStep: 2,
                lineColor: Theme.of(context)
                    .colorScheme
                    .primaryVariant
                    .withOpacity(0.2),
              ),
              VerticalAxisDecoration(
                endWithChart: false,
                lineWidth: 2.0,
                axisStep: 7,
                lineColor: Theme.of(context)
                    .colorScheme
                    .primaryVariant
                    .withOpacity(0.8),
              ),
              GridDecoration(
                endWithChart: false,
                showVerticalGrid: true,
                showHorizontalValues: false,
                showVerticalValues: true,
                verticalValuesPadding:
                    const EdgeInsets.symmetric(vertical: 12.0),
                verticalAxisStep: 1,
                horizontalAxisStep: 1,
                textStyle: Theme.of(context).textTheme.caption,
                gridColor: Theme.of(context)
                    .colorScheme
                    .primaryVariant
                    .withOpacity(0.2),
              ),
            ]),
      ),
    );
  }

  Widget _viewMeses(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 35),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              CircleAvatar(child: Text('Ene')),
              CircleAvatar(child: Text('Feb')),
              CircleAvatar(child: Text('Mar')),
              CircleAvatar(child: Text('Abr'))
            ],
          ),
          const SizedBox(height: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              CircleAvatar(child: Text('May')),
              CircleAvatar(child: Text('Jun')),
              CircleAvatar(child: Text('Jul')),
              CircleAvatar(child: Text('Ago'))
            ],
          ),
          const SizedBox(height: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              CircleAvatar(child: Text('Sep')),
              CircleAvatar(child: Text('Oct')),
              CircleAvatar(child: Text('Nov')),
              CircleAvatar(child: Text('Dic'))
            ],
          ),
        ],
      ),
    );
  }
}
