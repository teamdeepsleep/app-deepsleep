// To parse this JSON data, do
//
//     final especialistaResponse = especialistaResponseFromMap(jsonString);

import 'dart:convert';

import 'package:app_deepsleep/src/models/especialista_model.dart';

class ResponseEspecialista {
  ResponseEspecialista({
    required this.operacion,
    required this.data,
    required this.mensaje,
  });

  final bool operacion;
  final EspecialistaModel data;
  final String mensaje;

  factory ResponseEspecialista.fromJson(String str) =>
      ResponseEspecialista.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory ResponseEspecialista.fromMap(Map<String, dynamic> json) =>
      ResponseEspecialista(
        operacion: json["operacion"],
        data: EspecialistaModel.fromMap(json["data"]),
        mensaje: json["mensaje"],
      );

  Map<String, dynamic> toMap() => {
        "operacion": operacion,
        "data": data.toMap(),
        "mensaje": mensaje,
      };
}
