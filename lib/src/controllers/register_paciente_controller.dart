import 'package:app_deepsleep/src/models/models.dart';
import 'package:app_deepsleep/src/providers/login_provider.dart';
import 'package:app_deepsleep/src/utils/validaciones.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class RegisterPacienteController extends GetxController {
  final loginProvider = Get.find<LoginProvider>();

  TextEditingController nombre = TextEditingController();
  TextEditingController apellido = TextEditingController();
  TextEditingController dni = TextEditingController();
  TextEditingController corre = TextEditingController();
  TextEditingController telefono = TextEditingController();
  TextEditingController password = TextEditingController();
  bool isCreated = false;

  Future<bool> registrarPaciente() async {
    if (nombre.value.text.isEmpty ||
        apellido.value.text.isEmpty ||
        dni.value.text.isEmpty ||
        corre.value.text.isEmpty ||
        telefono.value.text.isEmpty ||
        password.value.text.isEmpty) {
      Get.snackbar('Registro', 'Porfavor revisar y completar todos los campos');
      return false;
    }

    var mensaje = '';
    if (nombre.value.text.length < 3) {
      mensaje = '$mensaje nombre';
    }
    if (apellido.value.text.length < 3) {
      mensaje = '$mensaje apellido';
    }
    if (dni.value.text.length != 8) {
      mensaje = '$mensaje dni';
    }
    if (isEmail(corre.value.text) == false) {
      mensaje = '$mensaje correo';
    }
    if (password.value.text.length < 8) {
      mensaje = '$mensaje password';
    }
    if (telefono.value.text.length != 9) {
      mensaje = '$mensaje telefono';
    }
    if (mensaje != '') {
      Get.snackbar('Registro', 'porfavor completar$mensaje correctamente"');
      return false;
    }
    cargando(true);
    var pacienteNEW = Paciente();
    pacienteNEW.nombre = nombre.value.text;
    pacienteNEW.apellidos = apellido.value.text;
    pacienteNEW.dni = dni.value.text;
    pacienteNEW.correo = corre.value.text;
    pacienteNEW.password = password.value.text;
    pacienteNEW.telefono = telefono.value.text;

    var isCreated = await loginProvider.registarNuevoPaciente(pacienteNEW);

    if (isCreated.operacion == true) {
      var storage = GetStorage();
      var token = 'isCreated.token';
      var usuarioTipo = 'usuario';
      var idUsuario = isCreated.data.id;

      storage.write('isLogued', true);
      storage.write('token', token);
      storage.write('usuarioTipo', usuarioTipo);
      storage.write('idUsuario', idUsuario);
    } else {
      Get.snackbar('Registro', 'datos incorrectos');
      return false;
    }
    cargando(false);
    return isCreated.operacion;
  }

  cargando(bool value) {
    isCreated = value;
    update();
  }
}
