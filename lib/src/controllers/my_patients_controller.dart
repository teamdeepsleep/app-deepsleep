import 'package:app_deepsleep/src/models/response/especialista_lista_response.dart';
import 'package:app_deepsleep/src/providers/especialista_provider.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class MyPatientsController extends GetxController {
  final especialistaProvider = Get.find<EspecialistaProvider>();
  MyPatientsController();

  Future<List<SolicitudModel>> listarMisPacientes() async {
    var storage = GetStorage();
    var idEspecialista = await storage.read('idUsuario');
    var lista = await especialistaProvider.listarMisPacientes(idEspecialista);
    print(lista);

    return lista;
  }

  actualizarVista() {
    update();
  }
}
