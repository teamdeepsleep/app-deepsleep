import 'package:app_deepsleep/src/utils/formatos.dart';
import 'package:flutter/material.dart';

class Alarm {
  final TimeOfDay timeInit;
  final TimeOfDay timeEnd;
  bool isActived = true;
  late bool isLunes;
  late bool isMartes;
  late bool isMiercoles;
  late bool isJueves;
  late bool isViernes;
  late bool isSabado;
  late bool isDomingo;

  Alarm({
    required this.timeInit,
    required this.timeEnd,
    required this.isLunes,
    required this.isMartes,
    required this.isMiercoles,
    required this.isJueves,
    required this.isViernes,
    required this.isSabado,
    required this.isDomingo,
  });

  String alarmaTextoInicial() {
    // return '${timeInit.hour}: ${timeInit.minute}';
    return '${textFormatoTiempo00('${timeInit.hour}')}: ${textFormatoTiempo00('${timeInit.minute}')}';
  }

  String alarmaTextoFinall() {
    return '${textFormatoTiempo00('${timeEnd.hour}')}: ${textFormatoTiempo00('${timeEnd.minute}')}';
  }

  
}
