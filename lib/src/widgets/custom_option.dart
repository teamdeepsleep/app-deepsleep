import 'package:app_deepsleep/src/themes/app_themes.dart';
import 'package:flutter/material.dart';

class CustomOption extends StatelessWidget {
  final IconData icon;
  final Color color;
  final String text;

  const CustomOption({
    Key? key,
    required this.icon,
    required this.color,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(15),
      height: 180,
      width: 150,
      decoration: BoxDecoration(
        color: const Color.fromRGBO(62, 66, 107, 0.33),
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CircleAvatar(
            backgroundColor: color,
            child: Icon(icon),
            radius: 30,
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            text,
            style: const TextStyle(color: AppTheme.primary, fontSize: 18),
          )
        ],
      ),
    );
  }
}
