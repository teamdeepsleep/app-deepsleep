import 'package:app_deepsleep/src/controllers/login_controller.dart';
import 'package:app_deepsleep/src/themes/app_themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var loginController = Get.put(LoginController());

    return GetBuilder<LoginController>(builder: (controller) {
      
      return Scaffold(
        backgroundColor: AppTheme.primary,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    alignment: Alignment.center,
                    child: Image.asset(
                      'assets/img/reading.png',
                      fit: BoxFit.cover,
                      height: 200,
                      width: 200,
                    ),
                  ),
                  const SizedBox(height: 40),
                  const Text(
                    'Correo',
                    style: TextStyle(fontSize: 20, color: Colors.white),
                  ),
                  const SizedBox(height: 15),
                  TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    controller: controller.email,
                    decoration: const InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        hintText: 'Ingrese un correo'),
                  ),
                  const SizedBox(height: 30),
                  const Text(
                    'Contraseña',
                    style: TextStyle(fontSize: 20, color: Colors.white),
                  ),
                  const SizedBox(height: 15),
                  TextFormField(
                    obscureText: true,
                    controller: loginController.password,
                    decoration: const InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        hintText: 'Ingrese su contraseña'),
                  ),
                  const SizedBox(height: 40),
                  Container(
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(25)),
                        color: Colors.white),
                    height: 45,
                    width: double.infinity,
                    child: InkWell(
                      onTap: () async {
                        var isLogin200 = await login(controller);
                        if (isLogin200 == 'usuario') {
                          Navigator.pushReplacementNamed(
                              context, 'home_paciente');
                        }
                        if (isLogin200 == 'especialista') {
                          Navigator.pushReplacementNamed(
                              context, 'home_especialista');
                        }
                      },
                      child: _crearBotonLogin(controller),
                    ),
                  ),
                  const SizedBox(height: 25),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        '¿No tienes una cuenta? ',
                        style: TextStyle(fontSize: 15, color: Colors.white),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.pushReplacementNamed(
                              context, 'register_option');
                        },
                        child: Text(
                          ' Regístrate aquí',
                          style: TextStyle(
                            fontStyle: FontStyle.italic,
                            fontSize: 15,
                            color: Colors.cyan[900],
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}

Future<String> login(LoginController controller) async {
  return await controller.login();
}

Widget _crearBotonLogin(LoginController controller) {
  if (controller.isLogin == true) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: const [
        SizedBox(width: 30, height: 30, child: CircularProgressIndicator()),
        Text('  Cargando ...')
      ],
    );
  } else {
    return const Center(
        child: Text(
      'Ingresar',
      style: TextStyle(fontSize: 20, color: AppTheme.primary),
    ));
  }
}
