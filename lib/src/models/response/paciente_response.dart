// To parse this JSON data, do
//
//     final pacienteResponse = pacienteResponseFromMap(jsonString);

import 'dart:convert';

class PacienteResponse {
  PacienteResponse({
    required this.id,
    required this.nombre,
    required this.apellido,
    required this.dni,
    required this.telefono,
    required this.correo,
    required this.recomendaciones,
    required this.v,
    required this.habilitado,
    this.imagen = '',
    required this.psqi,
  });

  late String id = '';
  late String nombre;
  late String apellido;
  late String dni;
  late String telefono;
  late String correo;
  late List<dynamic> recomendaciones;
  late int v;
  late bool habilitado;
  late String imagen;
  late int psqi;

  factory PacienteResponse.fromJson(String str) =>
      PacienteResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory PacienteResponse.fromMap(Map<String, dynamic> json) =>
      PacienteResponse(
        id: json["_id"],
        nombre: json["nombre"],
        apellido: json["apellido"],
        dni: json["dni"],
        telefono: json["telefono"] ?? '',
        correo: json["correo"],
        recomendaciones:
            List<dynamic>.from(json["recomendaciones"].map((x) => x)),
        v: json["__v"],
        habilitado: json["habilitado"],
        imagen: json["imagen"] ??
            'https://www.tenforums.com/geek/gars/images/2/types/thumb_15951118880user.png',
        psqi: json["psqi"],
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "nombre": nombre,
        "apellido": apellido,
        "dni": dni,
        "telefono": telefono,
        "correo": correo,
        "recomendaciones": List<dynamic>.from(recomendaciones.map((x) => x)),
        "__v": v,
        "habilitado": habilitado,
        "imagen": imagen,
        "psqi": psqi,
      };
}
