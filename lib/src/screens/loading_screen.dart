import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';

class LoadingScreen extends StatelessWidget {
  const LoadingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _validarTipoUsuario(context);

    return Scaffold(
      backgroundColor: Colors.black,
      body: InkWell(
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            /// Paint the area where the inner widgets are loaded with the
            /// background to keep consistency with the screen background
            Container(
              decoration: const BoxDecoration(color: Colors.black),
            ),

            /// Render the background image
            Image.asset('assets/img/fondo.jpeg', fit: BoxFit.cover),

            /// Render the Title widget, loader and messages below each other
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    // ignore: prefer_const_literals_to_create_immutables
                    children: <Widget>[
                      const Padding(
                        padding: EdgeInsets.only(top: 30.0),
                      ),
                      const Text(''),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    // ignore: prefer_const_literals_to_create_immutables
                    children: <Widget>[
                      /// Loader Animation Widget
                      const CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.green),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      const Text('Cargando ...'),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void _validarTipoUsuario(BuildContext context) async {
    Timer(const Duration(seconds: 2), () async {
      final box = GetStorage();
      var isLogued = box.read<bool>('isLogued');
      // var usuarioId = box.read('usuarioId');

      if (isLogued == true) {
        var usuarioTipo = box.read('usuarioTipo');

        if (usuarioTipo == 'usuario') {
          Navigator.pushReplacementNamed(context, 'home_paciente');
        }
        if (usuarioTipo == 'especialista') {
          Navigator.pushReplacementNamed(context, 'home_especialista');
        }
      } else {
        Navigator.pushReplacementNamed(context, 'login');
      }
    });
  }
}
