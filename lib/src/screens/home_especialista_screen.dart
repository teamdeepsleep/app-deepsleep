import 'package:app_deepsleep/src/controllers/bottom_navigation2_controller.dart';
import 'package:app_deepsleep/src/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_deepsleep/src/screens/screens.dart';

class HomeEspecialistaScreen extends StatelessWidget {
  const HomeEspecialistaScreen({Key? key}) : super(key: key);
  static List<StatelessWidget> tab = const [
    PotentialPatientsScreen(),
    ProfileEspecialistaScreen(),
    MyPatientsScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    Get.put(BottomNavigation2Controller());

    return GetBuilder<BottomNavigation2Controller>(builder: (controller) {
      return Scaffold(
        appBar: AppBar(
          title: Text(controller.tituloAppBar),
          actions: [_viewCerrarSesion(context, controller)],
        ),
        body: tab[controller.selectIndex],
        bottomNavigationBar: CustomBottomNavigationEspecialista(
          controller: controller,
        ),
      );
    });
  }
}

_viewCerrarSesion(
    BuildContext context, BottomNavigation2Controller controller) {
  if (controller.selectIndex == 1) {
    return InkWell(
      onTap: () async {
        displayDialogAndroidCerrarSesionEspecialista(context, controller);
      },
      child: Container(
        margin: const EdgeInsets.only(right: 5),
        child: const Icon(Icons.logout),
      ),
    );
  } else {
    return Container();
  }
}

displayDialogAndroidCerrarSesionEspecialista(
    BuildContext context, BottomNavigation2Controller controller) {
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadiusDirectional.circular(15)),
        elevation: 5,
        title: const Text('Cerrar Sesion'),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: const [
            Text('¿Desea cerrar sesión?'),
          ],
        ),
        actions: [
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: const Text('Cancelar'),
          ),
          TextButton(
            onPressed: () async {
              controller.logout();
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, 'login');
            },
            child: const Text('SI'),
          )
        ],
      );
    },
  );
}
