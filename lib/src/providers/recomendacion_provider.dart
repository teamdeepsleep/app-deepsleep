import 'package:app_deepsleep/src/environment/index.dart';
import 'package:app_deepsleep/src/models/recomendacion_model.dart';
import 'package:app_deepsleep/src/models/response/recomendacion_response.dart';
import 'package:get/get.dart';

class RecomendacionProvider extends GetConnect {
  static const apiUrl = Environment.baseUrl;
  @override
  void onInit() {}

  Future<List<Recomendacion>> listarMisRecomendaciones(id) async {
    
    // if (resp.statusCode == 200) {
    //   final recomendacionResponse = RecomendacionResponse.fromMap(resp.body);
    //   List<Recomendacion> recomendaciones = recomendacionResponse.data;
    //   return recomendaciones;
    // } else {
    //   return [];
    // }

    try {
      var url = '$apiUrl/recomendacion/paciente/$id';
      print(url);
      
      final resp = await get(url);
      final recomendacionResponse = RecomendacionResponse.fromMap(resp.body);
      return recomendacionResponse.data;
    } catch (e) {
      return [];
    }
  }

  
}
