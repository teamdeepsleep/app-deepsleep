import 'package:app_deepsleep/src/environment/index.dart';
import 'package:app_deepsleep/src/models/models.dart';
import 'package:app_deepsleep/src/models/response/login_response.dart';
import 'package:app_deepsleep/src/models/response/registro_especialista_response.dart';
import 'package:app_deepsleep/src/models/response/registro_paciente_response.dart';
import 'package:get/get_connect.dart';

class LoginProvider extends GetConnect {
  static const apiUrl = Environment.baseUrl;
  @override
  void onInit() {}

  Future<LoginResponse> validarLogin(String correo, password) async {
    const url = '$apiUrl/auth/login';
    final resp = await post(url, {"correo": correo, "password": password});
    print(resp);
    if (resp.hasError == true) {
      var resError = LoginResponse(
          operacion: false,
          mensaje: 'Intentelo otra vez',
          id: '',
          tipo: '',
          token: '');
      return resError;
    }
    final loginResponse = LoginResponse.fromMap(resp.body);
    return loginResponse;
  }

  Future<RegistroPacienteResponse> registarNuevoPaciente(
      Paciente paciente) async {
    const url = '$apiUrl/paciente';
    final resp = await post(url, {
      "nombre": paciente.nombre,
      "apellido": paciente.apellidos,
      "dni": paciente.dni,
      "correo": paciente.correo,
      "telefono": paciente.telefono,
      "password": paciente.password
    });
    final resppostPaciente = RegistroPacienteResponse.fromMap(resp.body);

    return resppostPaciente;
  }

  Future<RegistroEspecialistaResponse> registarNuevoEspecialista(
      Paciente especialista) async {
    const url = '$apiUrl/especialista';
    final resp = await post(url, {
      "nombre": especialista.nombre,
      "apellido": especialista.apellidos,
      "dni": especialista.dni,
      "correo": especialista.correo,
      "telefono": especialista.telefono,
      "password": especialista.password
    });
    final resppostEspecialista =
        RegistroEspecialistaResponse.fromMap(resp.body);

    return resppostEspecialista;
  }
}
