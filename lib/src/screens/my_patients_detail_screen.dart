import 'package:app_deepsleep/src/controllers/my_patients_detail_controller.dart';
import 'package:app_deepsleep/src/models/response/paciente_response.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MyPatientsDetailScreen extends StatelessWidget {
  const MyPatientsDetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(MyPatientsDetailController());
    var dataRecivida = ModalRoute.of(context)?.settings.arguments as String;
    var tipo = dataRecivida.substring(0, 1);
    var idPaciente = dataRecivida.substring(1);

    return GetBuilder<MyPatientsDetailController>(builder: (controller) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Detalle '),
        ),
        body: FutureBuilder(
          future: controller.buscarDetailPaciente(idPaciente),
          builder:
              (BuildContext context, AsyncSnapshot<PacienteResponse> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }

            if (snapshot.connectionState == ConnectionState.done) {
              PacienteResponse? dato = snapshot.data;

              if (dato == null) {
                return const Text('Default vista1');
              } else {
                return SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: NetworkImage(dato.imagen),
                                fit: BoxFit.cover)),
                        child: SizedBox(
                          width: double.infinity,
                          height: 200,
                          child: Container(
                            alignment: const Alignment(0.0, 2.5),
                            child: CircleAvatar(
                              backgroundImage: NetworkImage(dato.imagen),
                              radius: 60.0,
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 60,
                      ),
                      Text(
                        '${dato.nombre} ${dato.apellido} ',
                        style: const TextStyle(
                            fontSize: 25.0,
                            color: Colors.blueGrey,
                            letterSpacing: 2.0,
                            fontWeight: FontWeight.w400),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text(
                        'Celular',
                        style: TextStyle(
                            fontSize: 18.0,
                            color: Colors.black45,
                            letterSpacing: 2.0,
                            fontWeight: FontWeight.w300),
                      ),
                      Text(
                        dato.telefono,
                        style: const TextStyle(
                            fontSize: 18.0,
                            color: Colors.black45,
                            letterSpacing: 2.0,
                            fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text(
                        'DNI',
                        style: TextStyle(
                            fontSize: 18.0,
                            color: Colors.black45,
                            letterSpacing: 2.0,
                            fontWeight: FontWeight.w300),
                      ),
                      Text(
                        dato.dni,
                        style: const TextStyle(
                            fontSize: 18.0,
                            color: Colors.black45,
                            letterSpacing: 2.0,
                            fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text(
                        'PSQI',
                        style: TextStyle(
                            fontSize: 18.0,
                            color: Colors.black45,
                            letterSpacing: 2.0,
                            fontWeight: FontWeight.w300),
                      ),
                      Text(
                        '${dato.psqi}',
                        style: const TextStyle(
                            fontSize: 18.0,
                            color: Colors.black45,
                            letterSpacing: 2.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                );
              }
            }

            return const Text('MyPatientsDetailScreen');
          },
        ),
        floatingActionButton:
            _showFloatingBtn(context, controller, idPaciente, tipo),
      );
    });
  }
}

Widget _showFloatingBtn(BuildContext context,
    MyPatientsDetailController controller, String idPaciente, String tipo) {
  if (tipo == '1') {
    return FloatingActionButton(
        onPressed: () async {
          _displayDialogAndroidAceptarSolicitud(
              context, controller, idPaciente);
        },
        child: const Icon(Icons.swap_horizontal_circle));
  } else {
    return Container();
  }
}

_displayDialogAndroidAceptarSolicitud(BuildContext context,
    MyPatientsDetailController controller, String idPaciente) {
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadiusDirectional.circular(15)),
        elevation: 5,
        title: const Text('Invitación'),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: const [
            Text('¿Desea aceptar la invitación de ...?'),
          ],
        ),
        actions: [
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: const Text('Cancelar'),
          ),
          TextButton(
            onPressed: () async {
              //aceptarInivtacion
              var isAcepted =
                  await controller.aceptarInvitacionPorId(idPaciente);
              //regresar a la lista general
              Navigator.pop(context);

              if (isAcepted == true) {
                Navigator.pop(context, true);
              } else {
                Navigator.pop(context, false);
              }
            },
            child: const Text('SI'),
          )
        ],
      );
    },
  );
}
