import 'package:app_deepsleep/src/controllers/goal_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GoalScreen extends StatelessWidget {
  const GoalScreen({Key? key}) : super(key: key);

  void initState() {
    print('click en ');
  }

  @override
  Widget build(BuildContext context) {
    Get.put(GoalController());
    return GetBuilder<GoalController>(builder: (controller) {
      //   controller.buscarPSQI();
      return Scaffold(
        appBar: AppBar(
          title: const Text('Metas'),
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: (controller.psqi == 0)
                ? const Center(
                    child: Text('realizar encuesta'),
                  )
                : Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            // color: Colors.red,
                            height: 140,
                            width: 110,
                            child: Image.asset(
                              'assets/img/superhero.png',
                              fit: BoxFit.contain,
                              // height: 180,
                              // width: 180,
                            ),
                          ),
                          Container(
                            // color: Colors.amber,
                            child: Slider(
                                min: 0,
                                max: 200,
                                value: controller.psqiAux,
                                onChanged: (value) {
                                  controller.updatePsqiAux(value);
                                }),
                          )
                        ],
                      ),
                      const SizedBox(height: 20),
                      Row(
                        children: [
                          const Text('Calidad de sueño:',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                              )),
                          _valorTextoPSQI(controller),
                        ],
                      ),
                      Row(
                        children: [
                          const Text('Puntaje PSQI:',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                              )),
                          _valorDePSQI(controller),
                        ],
                      ),
                      const SizedBox(height: 20),
                      const Text('Deseo mejorar mi PSQI a una categoría de:'),
                      const SizedBox(height: 20),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          ElevatedButton(
                            child: const Text('Muy buena'),
                            onPressed: () {},
                          ),
                          ElevatedButton(
                            child: const Text('Buena'),
                            onPressed: () {},
                          )
                        ],
                      )
                    ],
                  ),
          ),
        ),
      );
    });
  }
}

_valorDePSQI(GoalController controller) {
  return Text('${controller.psqi}');
}

_valorTextoPSQI(GoalController controller) {
  int valor = controller.psqi;
//   0 <= 4 "Calidad del sueño muy buena"
// 5 <= 10 "Calidad del sueño buena"
// 11 <= 15 "Calidad del sueño mala"
// >15 "Calidad del sueño muy mala"

  if (valor <= 4) {
    return const Text('Calidad del sueño muy buena');
  } else if (5 <= valor && valor <= 10) {
    return const Text('Calidad del sueño buena');
  } else if (11 <= valor && valor <= 15) {
    return const Text('Calidad del sueño mala');
  } else {
    return const Text('Calidad del sueño muy mala');
  }
}
