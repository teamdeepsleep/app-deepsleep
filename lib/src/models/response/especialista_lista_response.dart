// To parse this JSON data, do
//
//     final responseEspsecialistaLista = responseEspsecialistaListaFromMap(jsonString);

import 'dart:convert';

class ResponseEspecialistaLista {
  ResponseEspecialistaLista({
    required this.operacion,
    required this.data,
    required this.mensaje,
  });

  final bool operacion;
  final List<SolicitudModel> data;
  final String mensaje;

  factory ResponseEspecialistaLista.fromJson(String str) =>
      ResponseEspecialistaLista.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory ResponseEspecialistaLista.fromMap(Map<String, dynamic> json) =>
      ResponseEspecialistaLista(
        operacion: json["operacion"],
        data: List<SolicitudModel>.from(
            json["data"].map((x) => SolicitudModel.fromMap(x))),
        mensaje: json["mensaje"],
      );

  Map<String, dynamic> toMap() => {
        "operacion": operacion,
        "data": List<dynamic>.from(data.map((x) => x.toMap())),
        "mensaje": mensaje,
      };
}

class SolicitudModel {
  SolicitudModel({
    this.id = '',
    required this.idPaciente,
    required this.idEspecialista,
    required this.estado,
    required this.fecha,
    required this.v,
  });

  late String id;
  final IdPaciente idPaciente;
  final String idEspecialista;
  final String estado;
  final DateTime fecha;
  final int v;

  factory SolicitudModel.fromJson(String str) =>
      SolicitudModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory SolicitudModel.fromMap(Map<String, dynamic> json) => SolicitudModel(
        id: json["_id"],
        idPaciente: IdPaciente.fromMap(json["idPaciente"]),
        idEspecialista: json["idEspecialista"],
        estado: json["estado"],
        fecha: DateTime.parse(json["fecha"]),
        v: json["__v"],
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "idPaciente": idPaciente.toMap(),
        "idEspecialista": idEspecialista,
        "estado": estado,
        "fecha": fecha.toIso8601String(),
        "__v": v,
      };
}

class IdPaciente {
  IdPaciente({
    required this.id,
    required this.nombre,
    required this.apellido,
    required this.dni,
    required this.correo,
    required this.imagen,
    required this.telefono,
  });

  final String id;
  final String nombre;
  final String apellido;
  final String dni;
  final String correo;
  final String imagen;
  final String telefono;

  factory IdPaciente.fromJson(String str) =>
      IdPaciente.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory IdPaciente.fromMap(Map<String, dynamic> json) => IdPaciente(
        id: json["_id"],
        nombre: json["nombre"],
        apellido: json["apellido"],
        dni: json["dni"],
        correo: json["correo"],
        imagen: json["imagen"] ??
            'https://www.tenforums.com/geek/gars/images/2/types/thumb_15951118880user.png',
        telefono: json["telefono"] ?? '',
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "nombre": nombre,
        "apellido": apellido,
        "dni": dni,
        "correo": correo,
        "imagen": imagen,
        "telefono": telefono,
      };
}
