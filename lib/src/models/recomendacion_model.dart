// To parse this JSON data, do
//
//     final habitoResponse = habitoResponseFromMap(jsonString);

import 'dart:convert';

class Recomendacion {
  Recomendacion({
    this.id = '',
    // required this.nombre,
    // required this.v,
    // required this.imagen,
    required this.titulo,
    required this.descripcion,
    required this.v,
    required this.habitos,
    required this.imagen,
  });

  late String id;
  final String titulo;
  final String descripcion;
  final int v;
  final List<String> habitos;
  final String imagen;

  factory Recomendacion.fromJson(String str) =>
      Recomendacion.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Recomendacion.fromMap(Map<String, dynamic> json) => Recomendacion(
        id: json["_id"],
        titulo: json["titulo"],
        descripcion: json["descripcion"],
        v: json["__v"],
        habitos: List<String>.from(json["habitos"].map((x) => x)),
        imagen: json["imagen"] ??
            'https://www.tenforums.com/geek/gars/images/2/types/thumb_15951118880user.png',
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "titulo": titulo,
        "descripcion": descripcion,
        "__v": v,
        "habitos": List<dynamic>.from(habitos.map((x) => x)),
        "imagen": imagen,
      };
}
