part of 'ui_bloc.dart';

@immutable
abstract class UiState {
  // final bool existUser;
  final int bottonNavigationCurrentIndex;

  const UiState(
    // this.existUser,
    this.bottonNavigationCurrentIndex,
  );
}

class UiInitialState extends UiState {
  const UiInitialState() : super(0);
}

class IndexBottomNavigationSetState extends UiState {
  final int index;
  const IndexBottomNavigationSetState(this.index) : super(index);
}
