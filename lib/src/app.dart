import 'package:app_deepsleep/src/routes/app_routes.dart';
import 'package:app_deepsleep/src/themes/app_themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // return MultiBlocProvider(
    //   providers: [
    //     BlocProvider(create: (_) => UiBloc()),
    //   ],
    //   child: MaterialApp(
    //     debugShowCheckedModeBanner: false,
    //     title: 'Material App',
    //     initialRoute: AppRoutes.initialRoute,
    //     routes: AppRoutes.routes,
    //     theme: AppTheme.lightTheme,
    //   ),
    // );

    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      initialRoute: AppRoutes.initialRoute,
      routes: AppRoutes.routes,
      theme: AppTheme.lightTheme,
    );
  }
}
