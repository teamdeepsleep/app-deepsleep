part of 'ui_bloc.dart';

@immutable
abstract class UiEvent {}

class UsuarioExisteEvent extends UiEvent {
  final bool existe;

  UsuarioExisteEvent(this.existe);
}

class CambiarBottomNavigationIndexEvent extends UiEvent {
  final int index;

  CambiarBottomNavigationIndexEvent(this.index);
}
