import 'package:app_deepsleep/src/models/especialista_model.dart';
import 'package:app_deepsleep/src/models/response/especialista_response.dart';
import 'package:app_deepsleep/src/providers/profile_provider.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class PerfilEspecialistaController extends GetxController {
  final profileProvider = Get.find<ProfileProvider>();
  EspecialistaModel especialistaEncontrado = EspecialistaModel(
      id: '',
      nombre: '',
      apellido: '',
      dni: '',
      correo: '',
      habilitado: false,
      v: 0,
      telefono: '');
  PerfilEspecialistaController() {
    buscarProfileEspecialista();
  }

  Future buscarProfileEspecialista() async {
    var storage = GetStorage();
    var idUsuario = await storage.read('idUsuario');

    ResponseEspecialista especialistaPorId =
        await profileProvider.buscarProfileEspecialista(idUsuario);

    especialistaEncontrado.nombre = especialistaPorId.data.nombre;
    especialistaEncontrado.apellido = especialistaPorId.data.apellido;
    especialistaEncontrado.dni = especialistaPorId.data.dni;
    especialistaEncontrado.correo = especialistaPorId.data.correo;
    especialistaEncontrado.imagen = especialistaPorId.data.imagen;
    especialistaEncontrado.telefono = especialistaPorId.data.telefono;

    update();
  }
}
