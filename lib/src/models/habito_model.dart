// To parse this JSON data, do
//
//     final habitoResponse = habitoResponseFromMap(jsonString);

import 'dart:convert';

class Habito {
  Habito({
    this.id = '',
    required this.nombre,
    required this.v,
    required this.imagen,
    this.estado = false,
  });

  late String id;
  final String nombre;
  final int v;
  final String imagen;
  late bool estado;

  factory Habito.fromJson(String str) => Habito.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Habito.fromMap(Map<String, dynamic> json) => Habito(
        id: json["_id"],
        nombre: json["nombre"],
        v: json["__v"] ?? 0,
        imagen: json["imagen"] ??
            'https://www.tenforums.com/geek/gars/images/2/types/thumb_15951118880user.png',
        estado: json["estado"] ?? false,
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "nombre": nombre,
        "__v": v,
        "imagen": imagen,
        "estado": estado,
      };
}
