import 'package:app_deepsleep/src/controllers/profile_especialista_controller.dart';
import 'package:app_deepsleep/src/models/especialista_model.dart';
import 'package:app_deepsleep/src/models/response/detalle_especialista_response.dart';
import 'package:app_deepsleep/src/themes/app_themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class EspecialistaDetalleScreen extends StatelessWidget {
  const EspecialistaDetalleScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(ProfileEspecialistaController());
    return GetBuilder<ProfileEspecialistaController>(builder: (controller) {
      var especialista =
          ModalRoute.of(context)?.settings.arguments as DetalleEspecialista;
      //   var dsg = params['ss'];
      //   var idEspecialista = params!['idEspecialista'];
      return Scaffold(
        appBar: AppBar(
          title: const Text('detalle médico'),
        ),
        body: SafeArea(
          child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(especialista.imagen),
                        fit: BoxFit.cover)),
                child: SizedBox(
                  width: double.infinity,
                  height: 200,
                  child: Container(
                    alignment: const Alignment(0.0, 2.5),
                    child: CircleAvatar(
                      backgroundImage: NetworkImage(especialista.imagen),
                      radius: 60.0,
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 60),
              Text(
                '${especialista.nombre} ${especialista.apellido}',
                style: const TextStyle(
                    fontSize: 25.0,
                    color: Colors.blueGrey,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.w400),
              ),
              const SizedBox(height: 10),
              const Text(
                'Celular',
                style: TextStyle(
                    fontSize: 18.0,
                    color: Colors.black45,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.w300),
              ),
              Text(
                especialista.telefono,
                style: const TextStyle(
                    fontSize: 18.0,
                    color: Colors.blueGrey,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 10),
              const Text(
                'DNI',
                style: TextStyle(
                    fontSize: 18.0,
                    color: Colors.black45,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.w300),
              ),
              Text(
                especialista.dni,
                style: const TextStyle(
                    fontSize: 18.0,
                    color: Colors.blueGrey,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 10),
              const Text(
                'Correo',
                style: AppTheme.textStyle2,
              ),
              Text(
                especialista.correo,
                style: AppTheme.textStyle,
              ),
              const SizedBox(height: 10),
              const Text(
                'Centro Laboral',
                style: AppTheme.textStyle2,
              ),
              Text(
                especialista.trabajo,
                style: AppTheme.textStyle,
              ),
              const SizedBox(height: 10),
              const Text(
                'Especialidades',
                style: AppTheme.textStyle2,
              ),
              Expanded(
                  child: ListView.builder(
                      // scrollDirection: Axis.vertical,
                      // shrinkWrap: false,
                      itemCount: especialista.especialidades.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          title: Text(
                            especialista.especialidades[index],
                            style: AppTheme.textStyle,
                          ),
                        );
                      }))
            ],
          ),
        ),
      );
    });
  }
}
