import 'package:flutter/material.dart';

class RegisterOptionScreen extends StatelessWidget {
  const RegisterOptionScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: const Color.fromRGBO(255, 216, 137, 6),
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              Container(
                height: 200,
                width: 200,
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/img/superhero.png'),
                        fit: BoxFit.fill)),
              ),
              const SizedBox(height: 10),
              const Text(
                'TIPO DE USUARIO',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                    fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 70),
              _buttomUsuario(context),
              const SizedBox(height: 20),
              _buttomEspecialista(context),
              const SizedBox(height: 50),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buttomUsuario(BuildContext context) {
    return SizedBox(
      width: 300,
      height: 50,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25.0),
          border: Border.all(width: 4, color: Colors.white),
        ),
        child: ElevatedButton(
          child: const Text('Universitario', style: TextStyle(fontSize: 18.0)),
          onPressed: () {
            Navigator.pushReplacementNamed(context, 'register_paciente');
          },
        ),
      ),
    );
  }

  Widget _buttomEspecialista(BuildContext context) {
    return SizedBox(
      width: 300,
      height: 50,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25.0),
          border: Border.all(width: 4, color: Colors.white),
        ),
        child: ElevatedButton(
          child: const Text('Especialista', style: TextStyle(fontSize: 18.0)),
          onPressed: () {
            Navigator.pushReplacementNamed(context, 'register_especialista');
          },
        ),
      ),
    );
  }
}
