import 'package:app_deepsleep/src/models/response/detalle_especialista_response.dart';
import 'package:app_deepsleep/src/providers/especialista_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class DatosEspecialistasController extends GetxController {
  final especialistaProvider = Get.find<EspecialistaProvider>();

  TextEditingController telefono = TextEditingController();
  TextEditingController centroLaboral = TextEditingController();
  TextEditingController nuevaEspecialidad = TextEditingController();
  DatosEspecialistasController();

  Future<ResponseDetalleEspecialista> datosDetalleEspecialista() async {
    var storage = GetStorage();
    var idEspecialista = await storage.read('idUsuario');

    ResponseDetalleEspecialista res =
        await especialistaProvider.datosDetalleEspecialista(idEspecialista);

    return res;
  }

  Future<bool> actualizarEspecialista(String campo) async {
    var storage = GetStorage();
    var idEspecialista = await storage.read('idUsuario');
    bool res = false;

    if (campo == 'trabajo') {
      res = await especialistaProvider.actualizarEspecialista(
          idEspecialista, campo, centroLaboral.value.text);
    }

    if (campo == 'telefono') {
      res = await especialistaProvider.actualizarEspecialista(
          idEspecialista, campo, telefono.value.text);
    }
    update();
    return res;
  }

  Future<bool> agregarEspecialidad() async {
    var storage = GetStorage();
    var idEspecialista = await storage.read('idUsuario');
    var textEspecialidad = nuevaEspecialidad.value.text;

    var res = await especialistaProvider.agregarEspecialidad(
        idEspecialista, textEspecialidad);

    nuevaEspecialidad.text = "";
    update();
    return res;
  }

  Future<bool> eliminarEspecialidad(int index) async {
    var storage = GetStorage();
    var idEspecialista = await storage.read('idUsuario');

    var res =
        await especialistaProvider.eliminarEspecialidad(idEspecialista, index);

    update();
    return res;
  }
}
