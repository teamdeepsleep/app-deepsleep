import 'package:flutter/material.dart';

class CustomItemNotificationScreen extends StatelessWidget {
   
  const CustomItemNotificationScreen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
         child: Text('CustomItemNotificationScreen'),
      ),
    );
  }
}