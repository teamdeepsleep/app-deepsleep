import 'package:app_deepsleep/src/controllers/register_especialista_controller.dart';
import 'package:app_deepsleep/src/themes/app_themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegisterEspecialistaScreen extends StatelessWidget {
  const RegisterEspecialistaScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(RegisterEspecialistaController());
    return GetBuilder<RegisterEspecialistaController>(builder: (controller) {
      return Scaffold(
        backgroundColor: AppTheme.primary,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    alignment: Alignment.center,
                    child: Image.asset(
                      'assets/img/reading.png',
                      fit: BoxFit.cover,
                      height: 180,
                      width: 180,
                    ),
                  ),
                  const SizedBox(height: 15),
                  Container(
                    alignment: Alignment.center,
                    child: const Text(
                      'Registrar Especialista',
                      style: TextStyle(fontSize: 25, color: Colors.white),
                    ),
                  ),
                  const SizedBox(height: 40),
                  TextFormField(
                    controller: controller.nombre,
                    decoration: const InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        hintText: 'Ingrese su nombre'),
                  ),
                  const SizedBox(height: 30),
                  TextFormField(
                    controller: controller.apellido,
                    decoration: const InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        hintText: 'Ingrese sus apellidos'),
                  ),
                  const SizedBox(height: 30),
                  TextFormField(
                    maxLength: 8,
                    keyboardType: TextInputType.number,
                    controller: controller.dni,
                    decoration: const InputDecoration(
                        counter: Offstage(),
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        hintText: 'Ingrese su dni'),
                  ),
                  const SizedBox(height: 30),
                  TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    controller: controller.correo,
                    decoration: const InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        hintText: 'Ingrese su correo'),
                  ),
                  const SizedBox(height: 30),
                  TextFormField(
                    maxLength: 9,
                    keyboardType: TextInputType.phone,
                    controller: controller.telefono,
                    decoration: const InputDecoration(
                        counter: Offstage(),
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        hintText: 'Ingrese su telefono'),
                  ),
                  const SizedBox(height: 30),
                  TextFormField(
                    controller: controller.password,
                    obscureText: true,
                    decoration: const InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        hintText: 'Ingrese su Contraseña'),
                  ),
                  const SizedBox(height: 30),
                  InkWell(
                    onTap: () async {
                      var isCreted = await controller.registrarEspecialista();
                      if (isCreted == true) {
                        Navigator.pushReplacementNamed(
                            context, 'home_especialista');
                      }
                    },
                    child: Container(
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(25)),
                          color: Colors.white),
                      height: 45,
                      width: double.infinity,
                      child: _crearBotonRegistroEspecialista(controller),
                    ),
                  ),
                  const SizedBox(height: 15),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        '¿Ya tienes una cuenta?',
                        style: TextStyle(fontSize: 15, color: Colors.white),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.pushReplacementNamed(context, 'login');
                        },
                        child: Text(
                          ' Ingresa aquí',
                          style: TextStyle(
                            fontStyle: FontStyle.italic,
                            fontSize: 15,
                            color: Colors.cyan[900],
                          ),
                        ),
                      )
                    ],
                  ),
                  const SizedBox(height: 30),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}

Widget _crearBotonRegistroEspecialista(
    RegisterEspecialistaController controller) {
  if (controller.isCreated == true) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: const [
        SizedBox(width: 30, height: 30, child: CircularProgressIndicator()),
        Text('  Cargando ...')
      ],
    );
  } else {
    return const Center(
        child: Text(
      'REGISTRO',
      style: TextStyle(fontSize: 20, color: AppTheme.primary),
    ));
  }
}
