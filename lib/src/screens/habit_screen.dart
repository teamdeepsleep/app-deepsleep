import 'package:app_deepsleep/src/controllers/habit_controller.dart';
import 'package:app_deepsleep/src/controllers/recommendation_controller.dart';
import 'package:app_deepsleep/src/models/habito_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HabitScreen extends StatelessWidget {
  const HabitScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final size = MediaQuery.of(context).size;
    Get.put(HabitoController());
    return GetBuilder<HabitoController>(builder: (controller) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Hábitos'),
        ),
        body: SafeArea(
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.all(15),
                child: const Text(
                    'Selecciona los hábitos de consumo que frecuentemente realizas'),
              ),
              Expanded(
                  child: Container(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GridView.builder(
                      itemCount: controller.habitos.length,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              crossAxisSpacing: 4.0,
                              mainAxisSpacing: 4.0),
                      itemBuilder: (context, index) {
                        return Container(
                          width: 200,
                          height: 200,
                          //   color: Colors.amber,
                          child: _cardHabito(
                              controller.habitos[index], index, controller),
                        );
                      }),
                ),
              )),
              Container(
                width: double.infinity,
                margin: const EdgeInsets.all(8),
                child: InkWell(
                  onTap: () {},
                  child: ElevatedButton(
                    onPressed: () async {
                      await controller.guardarMisHabitos();
                      var f = Get.put(RecommendationController());
                      f.listarMisRecomendaciones();
                    },
                    child: _loadingHabit(controller),
                  ),
                ),
              )
            ],
          ),
        ),
      );
    });
  }
}

Widget _loadingHabit(HabitoController controller) {
  if (controller.isLoading == true) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: const [
        SizedBox(width: 30, height: 30, child: CircularProgressIndicator()),
        Text('  Cargando ...')
      ],
    );
  } else {
    return const Center(
        child: Text(
      'GUARDAR',
      style: TextStyle(fontSize: 20),
    ));
  }
}

Widget _cardHabito(Habito habito, int index, HabitoController controller) {
  return Column(
    children: [
      //   Image.asset(
      //     'http://res.cloudinary.com/di8bhgjd5/image/upload/v1649602792/deepsleep/paciente/vvrvssdyeehtxscyxtaq.png',
      //     fit: BoxFit.contain,
      //     height: 120,
      //     width: 180,
      //   ),
      FadeInImage(
        image: NetworkImage(habito.imagen),
        placeholder: const AssetImage('assets/img/loading.gif'),
        fit: BoxFit.contain,
        height: 120,
        width: 180,
      ),
      Checkbox(
          value: habito.estado,
          onChanged: (check) {
            controller.actualizarCheckHabito(index, check!);
          })
    ],
  );
}
