import 'package:app_deepsleep/src/models/recomendacion_model.dart';
import 'package:app_deepsleep/src/themes/app_themes.dart';
import 'package:flutter/material.dart';

class RecommendationDetailScreen extends StatelessWidget {
  const RecommendationDetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var recomendacion =
        ModalRoute.of(context)?.settings.arguments as Recomendacion;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Detalle de la recomendación'),
      ),
      body: Container(
        padding: const EdgeInsets.all(15.0),
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              alignment: const Alignment(0.0, 2.5),
              child: CircleAvatar(
                backgroundImage: NetworkImage(recomendacion.imagen),
                radius: 60.0,
              ),
            ),
            const SizedBox(height: 60),
            Text(
              recomendacion.titulo,
              style: AppTheme.textStyle2,
              textAlign: TextAlign.center,
            ),
            Text(
              recomendacion.descripcion,
              style: AppTheme.textStyle,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
