import 'package:app_deepsleep/src/models/habito_model.dart';
import 'package:app_deepsleep/src/providers/habito_provider.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class HabitoController extends GetxController {
  final habitos = <Habito>[];
  final habitoProvider = Get.find<HabitoProvider>();
  bool isLoading = false;

  HabitoController() {
    listarHabitos();
  }

  Future listarHabitos() async {
    var storage = GetStorage();
    var idUsuario = await storage.read('idUsuario');
    final habitosEncontrados = await habitoProvider.listarMisHabitos(idUsuario);
    habitos.addAll(habitosEncontrados);
    update();
  }

  actualizarCheckHabito(int index, bool check) {
    habitos[index].estado = check;
    update();
  }

  guardarMisHabitos() async {
    var storage = GetStorage();
    var idUsuario = await storage.read('idUsuario');

    var misD = <String>[];

    for (var element in habitos) {
      if (element.estado == true) {
        misD.add(element.id);
      }
    }
    cargando(true);
    await habitoProvider.guardarMisHabitos(idUsuario, misD, habitos);
    cargando(false);
    Get.snackbar('Hábitos', 'Se guardaron los hábitos');
  }

  cargando(bool value) {
    isLoading = value;
    update();
  }
}
