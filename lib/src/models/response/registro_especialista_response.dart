// To parse this JSON data, do
//
//     final registroEspecialistaResponse = registroEspecialistaResponseFromMap(jsonString);

import 'dart:convert';

class RegistroEspecialistaResponse {
  RegistroEspecialistaResponse({
    required this.operacion,
    required this.data,
  });

  final bool operacion;
  final EspecialistaRESPOST data;

  factory RegistroEspecialistaResponse.fromJson(String str) =>
      RegistroEspecialistaResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory RegistroEspecialistaResponse.fromMap(Map<String, dynamic> json) =>
      RegistroEspecialistaResponse(
        operacion: json["operacion"],
        data: EspecialistaRESPOST.fromMap(json["data"]),
      );

  Map<String, dynamic> toMap() => {
        "operacion": operacion,
        "data": data.toMap(),
      };
}

class EspecialistaRESPOST {
  EspecialistaRESPOST({
    required this.nombre,
    required this.apellido,
    required this.dni,
    required this.telefono,
    required this.correo,
    required this.password,
    required this.habilitado,
    required this.id,
    required this.v,
  });

  final String nombre;
  final String apellido;
  final String dni;
  final String telefono;
  final String correo;
  final String password;
  final bool habilitado;
  final String id;
  final int v;

  factory EspecialistaRESPOST.fromJson(String str) =>
      EspecialistaRESPOST.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory EspecialistaRESPOST.fromMap(Map<String, dynamic> json) =>
      EspecialistaRESPOST(
        nombre: json["nombre"],
        apellido: json["apellido"],
        dni: json["dni"],
        telefono: json["telefono"],
        correo: json["correo"],
        password: json["password"],
        habilitado: json["habilitado"],
        id: json["_id"],
        v: json["__v"],
      );

  Map<String, dynamic> toMap() => {
        "nombre": nombre,
        "apellido": apellido,
        "dni": dni,
        "telefono": telefono,
        "correo": correo,
        "password": password,
        "habilitado": habilitado,
        "_id": id,
        "__v": v,
      };
}
