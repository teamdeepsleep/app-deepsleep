import 'package:app_deepsleep/src/screens/screens.dart';
import 'package:flutter/material.dart';

class AppRoutes {
  static const initialRoute = 'loading';

  static Map<String, Widget Function(BuildContext)> routes = {
    //login
    'loading': (BuildContext context) => const LoadingScreen(),
    'login': (BuildContext context) => const LoginScreen(),
    'register_option': (BuildContext context) => const RegisterOptionScreen(),
    'register_paciente': (BuildContext context) =>
        const RegisterPacienteScreen(),
    'register_especialista': (BuildContext context) =>
        const RegisterEspecialistaScreen(),
    //pacientes
    'home_paciente': (BuildContext context) => const HomePacienteScreen(),
    'specialists': (BuildContext context) => const SpecialistsScreen(),
    'specialist_detalle': (BuildContext context) =>
        const EspecialistaDetalleScreen(),
    'mi_specialit': (BuildContext context) => const MiEspecialistaScreen(),
    'message_paciente': (BuildContext context) => const MessagePacienteScreen(),
    'recommendation': (BuildContext context) => const RecommendationScreen(),
    'recommendation_detail': (BuildContext context) =>
        const RecommendationDetailScreen(),
    'quiz': (BuildContext context) => const QuizScreen(),
    'profile_paciente': (BuildContext context) => const ProfilePacienteScreen(),
    'profile_edit': (BuildContext context) => const ProfileEditScreen(),
    'goal': (BuildContext context) => const GoalScreen(),
    'habit': (BuildContext context) => const HabitScreen(),
    'schedelu_datos': (BuildContext context) => const ScheduleDatosScreen(),
    // 'schedelu': (BuildContext context) => const ScheduleScreen(),

    //especialistas
    'home_especialista': (BuildContext context) =>
        const HomeEspecialistaScreen(),
    'especialista_datos': (BuildContext context) =>
        const DatosEspecialistaScreen(),
    'mi_patients_detail': (BuildContext context) =>
        const MyPatientsDetailScreen(),
  };
}
