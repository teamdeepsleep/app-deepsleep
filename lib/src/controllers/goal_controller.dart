// import 'package:flutter/material.dart';
import 'package:app_deepsleep/src/models/response/paciente_response.dart';
import 'package:app_deepsleep/src/providers/profile_provider.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class GoalController extends GetxController {
  final profileProvider = Get.find<ProfileProvider>();
  int psqi = 0;
  double psqiAux = 0;

  GoalController() {
    buscarPSQI();
  }

  Future buscarPSQI() async {
    var storage = GetStorage();
    var idUsuario = await storage.read('idUsuario');

    PacienteResponse pacientePorId =
        await profileProvider.buscarProfilePaciente(idUsuario);

    psqi = pacientePorId.psqi;
    psqiAux = pacientePorId.psqi + 0.00;
    print('el aux es---- ${psqiAux}');
    update();
  }

  updatePsqiAux(double value) {
    psqiAux = value;
    update();
  }

  updatePsqi(int value) {
    psqi = value;
    update();
  }
}
