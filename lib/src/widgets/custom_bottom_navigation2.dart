import 'package:app_deepsleep/src/controllers/bottom_navigation2_controller.dart';
import 'package:flutter/material.dart';

class CustomBottomNavigationEspecialista extends StatelessWidget {
  final BottomNavigation2Controller controller;

  const CustomBottomNavigationEspecialista({Key? key, required this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // UiBloc uiBloc = BlocProvider.of<UiBloc>(context);
    return BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: controller.selectIndex,
        onTap: (i) {
          controller.changeIndex(i);
        },
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.people),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.people),
            label: '',
          ),
        ]);
  }
}
