// To parse this JSON data, do
//
//     final habitoResponse = habitoResponseFromMap(jsonString);

import 'dart:convert';

import 'package:app_deepsleep/src/models/recomendacion_model.dart';

class RecomendacionResponse {
  RecomendacionResponse({
    required this.operacion,
    required this.data,
    required this.mensaje,
  });

  final bool operacion;
  final List<Recomendacion> data;
  final String mensaje;

  factory RecomendacionResponse.fromJson(String str) =>
      RecomendacionResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory RecomendacionResponse.fromMap(Map<String, dynamic> json) =>
      RecomendacionResponse(
        operacion: json["operacion"],
        data: List<Recomendacion>.from(
            json["data"].map((x) => Recomendacion.fromMap(x))),
        mensaje: json["mensaje"],
      );

  Map<String, dynamic> toMap() => {
        "operacion": operacion,
        "data": List<dynamic>.from(data.map((x) => x.toMap())),
        "mensaje": mensaje,
      };
}
