import 'package:app_deepsleep/src/models/response/especialista_lista_response.dart';
import 'package:app_deepsleep/src/providers/especialista_provider.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class PotentialPatientsController extends GetxController {
  final especialistaProvider = Get.find<EspecialistaProvider>();
  PotentialPatientsController() {
    listarMisPotencialesPacientes();
  }

  Future<List<SolicitudModel>> listarMisPotencialesPacientes() async {
    var storage = GetStorage();
    var idUsuario = await storage.read('idUsuario');
    var lista =
        await especialistaProvider.listarMisPotencialesPacientes(idUsuario);
    print(lista);

    return lista;
  }

  actualizarVista() {
    update();
  }
}
