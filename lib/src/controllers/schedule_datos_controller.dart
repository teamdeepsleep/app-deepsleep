import 'package:app_deepsleep/src/models/alarm_model.dart';
import 'package:app_deepsleep/src/utils/formatos.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ScheduleDatosController extends GetxController {
  TimeOfDay timeIni = TimeOfDay.now();
  TimeOfDay timeFin = TimeOfDay.now();
  bool isLunes = false;
  bool isMartes = false;
  bool isMiercoles = false;
  bool isJueves = false;
  bool isViernes = false;
  bool isSabado = false;
  bool isDomingo = false;

  TextEditingController textHoraInicio = TextEditingController();
  TextEditingController textHoraFin = TextEditingController();

  updateTimes(TimeOfDay time, pregunta) {
    if (pregunta == 1) {
      timeIni = time;
      textHoraInicio.text =
          '${textFormatoTiempo00('${time.hour}')} : ${textFormatoTiempo00('${time.minute}')}';
    }

    if (pregunta == 2) {
      timeFin = time;
      textHoraFin.text =
          '${textFormatoTiempo00('${time.hour}')} : ${textFormatoTiempo00('${time.minute}')}';
    }
    update();
  }

  updateDays(bool estado, String day) {
    switch (day) {
      case 'L':
        isLunes = !estado;
        break;
      case 'Ma':
        isMartes = !estado;
        break;
      case 'Mi':
        isMiercoles = !estado;
        break;
      case 'J':
        isJueves = !estado;
        break;
      case 'V':
        isViernes = !estado;
        break;
      case 'S':
        isSabado = !estado;
        break;
      case 'D':
        isDomingo = !estado;
        break;
      default:
    }

    update();
  }

  Alarm enviarData() {
    return Alarm(
        timeInit: timeIni,
        timeEnd: timeFin,
        isLunes: isLunes,
        isMartes: isMartes,
        isMiercoles: isMiercoles,
        isJueves: isJueves,
        isViernes: isViernes,
        isSabado: isSabado,
        isDomingo: isDomingo);
  }

  bool validarDatos() {
    if (textHoraInicio.text == '' || textHoraFin.text == '') {
      Get.snackbar('datos', 'completar los datos');
      return false;
    }

    return true;
  }

  limpiarData() {
    textHoraInicio.text = '';
    textHoraFin.text = '';
    timeIni = TimeOfDay.now();
    timeFin = TimeOfDay.now();
  }
}
