import 'package:app_deepsleep/src/controllers/my_patients_controller.dart';
import 'package:app_deepsleep/src/models/response/especialista_lista_response.dart';
import 'package:app_deepsleep/src/themes/app_themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MyPatientsScreen extends StatelessWidget {
  const MyPatientsScreen({Key? key}) : super(key: key);
// MyPatientsController
  @override
  Widget build(BuildContext context) {
    Get.put(MyPatientsController());
    return GetBuilder<MyPatientsController>(builder: (controller) {
      return Scaffold(
        body: FutureBuilder(
          future: controller.listarMisPacientes(),
          builder: (BuildContext context,
              AsyncSnapshot<List<SolicitudModel>> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }

            if (snapshot.connectionState == ConnectionState.done) {
              List<SolicitudModel>? datos = snapshot.data;
              if (datos == null) {
                return const Text('Default vista1');
              } else {
                if (datos.isEmpty) {
                  return const Center(child: Text('No tiene Pacientes'));
                } else {
                  return ListView.builder(
                      itemCount: datos.length,
                      itemBuilder: (context, index) {
                        return _item(datos[index], index, context, controller);
                      });
                }
              }
            }

            return const Text('Default vista2');
          },
        ),
      );
    });
  }
}

Widget _item(SolicitudModel solicitud, int i, BuildContext context,
    MyPatientsController controller) {
  return InkWell(
    onTap: () async {
      var enviarDATA = '3' + solicitud.idPaciente.id;
      var resultBack = await Navigator.pushNamed(context, 'mi_patients_detail',
          arguments: enviarDATA);
      if (resultBack != null) {
        print('actualizar');
        controller.actualizarVista();

        if (resultBack == true) {
          Get.snackbar('Solicitud', 'Se acpeto la solicitud');
        } else {
          Get.snackbar('Solicitud', 'No se Proceso');
        }
      }
    },
    child: Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      elevation: 15,
      shadowColor: AppTheme.primary.withOpacity(0.4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          FadeInImage(
            image: NetworkImage(solicitud.idPaciente.imagen),
            placeholder: const AssetImage('assets/img/loading.gif'),
            width: 80,
            height: 80,
            fit: BoxFit.cover,
          ),
          Column(
            children: [
              Container(
                // color: Colors.red,
                // height: 80,
                //   width: 169,
                child: Center(
                  child: Text(
                      '${solicitud.idPaciente.nombre} ${solicitud.idPaciente.apellido}'),
                ),
              ),
              Container(
                // color: Colors.red,
                // height: 80,
                //   width: 169,
                child: Center(
                  child: Text('${solicitud.idPaciente.correo} '),
                ),
              )
            ],
          ),
          const SizedBox(
            width: 10,
          )
        ],
      ),
    ),
  );
}
