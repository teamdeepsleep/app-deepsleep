// To parse this JSON data, do
//
//     final habitoResponse = habitoResponseFromMap(jsonString);

import 'dart:convert';

import 'package:app_deepsleep/src/models/habito_model.dart';

class HabitoResponse {
  HabitoResponse({
    required this.operacion,
    required this.data,
    required this.mensaje,
  });

  final bool operacion;
  final List<Habito> data;
  final String mensaje;

  factory HabitoResponse.fromJson(String str) =>
      HabitoResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory HabitoResponse.fromMap(Map<String, dynamic> json) => HabitoResponse(
        operacion: json["operacion"],
        data: List<Habito>.from(json["data"].map((x) => Habito.fromMap(x))),
        mensaje: json["mensaje"],
      );

  Map<String, dynamic> toMap() => {
        "operacion": operacion,
        "data": List<dynamic>.from(data.map((x) => x.toMap())),
        "mensaje": mensaje,
      };
}
