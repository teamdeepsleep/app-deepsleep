import 'package:app_deepsleep/src/controllers/mi_especialista_controller.dart';
import 'package:app_deepsleep/src/themes/app_themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MiEspecialistaScreen extends StatelessWidget {
  const MiEspecialistaScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(MiEspecialistaController());
    return GetBuilder<MiEspecialistaController>(builder: (controller) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Mi Médico'),
        ),
        body: Center(
          child: FutureBuilder(
              future: controller.estadoMiEspecialista(),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.data == null) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
                var datos = snapshot.data;

                if (datos["operacion"] == false) {
                  return Text(datos["mensaje"]);
                }
                return Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Container(
                    width: double.infinity,
                    child: Card(
                      clipBehavior: Clip.antiAlias,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      elevation: 15,
                      shadowColor: AppTheme.primary.withOpacity(0.4),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Column(
                              children: [
                                const Text(
                                  'ESTADO',
                                  style: TextStyle(
                                      fontSize: 18.0,
                                      color: Colors.black45,
                                      letterSpacing: 2.0,
                                      fontWeight: FontWeight.w300),
                                ),
                                const SizedBox(height: 10),
                                Text(
                                  datos["mensaje"],
                                  style: const TextStyle(
                                      fontSize: 18.0,
                                      color: Colors.black45,
                                      letterSpacing: 2.0,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            // const SizedBox(
                            //   height: 60),
                            (datos['mensaje'] == 'ACEPTADO')
                                ? Container()
                                : const Text(
                                    '¡Su solicitud será respondida en las próximas horas por ',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 18.0,
                                  color: Colors.black45,
                                  letterSpacing: 2.0,
                                  fontWeight: FontWeight.w300),
                            ),
                            Column(
                              children: [
                                Container(
                                  alignment: const Alignment(0.0, 2.5),
                                  child: CircleAvatar(
                                    backgroundImage:
                                        NetworkImage(datos["medico_imagen"]),
                                    radius: 60.0,
                                  ),
                                ),
                                const SizedBox(height: 20),
                                Text(
                                  datos["medico_nombre"],
                                  style: AppTheme.textStyle,
                                ),
                                const SizedBox(height: 8),
                                Text(
                                  datos["medico_correo"],
                                  style: AppTheme.textStyle,
                                ),
                                const SizedBox(height: 8),
                                Text(
                                  datos["medico_telefono"],
                                  style: AppTheme.textStyle,
                                )
                              ],
                            ),
                            //   Text('MiEspecialistaScreen')
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              }),
        ),
      );
    });
  }
}
