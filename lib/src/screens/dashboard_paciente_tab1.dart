import 'package:app_deepsleep/src/themes/app_themes.dart';
import 'package:flutter/material.dart';
import 'package:charts_painter/chart.dart';

class DashboardPacienteTab1Screen extends StatelessWidget {
  const DashboardPacienteTab1Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
          child: Stack(
        children: [
          Column(
            children: [
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                color: AppTheme.primary,
                width: double.infinity,
                height: 50,
                child: Text('Duración promedio de sueño: 4h 30 min'),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: size.width * 0.45,
                      height: 50,
                      margin: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 5),
                      color: AppTheme.primary,
                      child: Text('En promedio duerme a las 23: 30'),
                    ),
                    Container(
                      width: size.width * 0.45,
                      height: 50,
                      margin: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 5),
                      color: AppTheme.primary,
                      child: Text('En promedio despierta a las 04:00'),
                    ),
                  ],
                ),
              ),
              _viewDashBoard(context),
              _viewMeses(context),
            ],
          )
        ],
      )),
    );
  }

  Widget _viewDashBoard(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10),
      width: double.infinity,
      height: 300.0,
      // color: Colors.yellow,
      child: Chart(
        state: ChartState.bar(
            ChartData.fromList(
              <double>[
                1,
                3,
                4,
                2,
                7,
                6,
                2,
              ].map((e) => BarValue<void>(e)).toList(),
            ),
            foregroundDecorations: [
              BorderDecoration(borderWidth: 1.0),

              /// ADDED: Add spark line decoration ([SparkLineDecoration]) on foreground
              // SparkLineDecoration(),
            ],
            //   itemOptions: BubbleItemOptions(
            //       // padding: const EdgeInsets.symmetric(horizontal: 4.0),
            //       // maxBarWidth: 10,
            //       ),
            //   behaviour: ChartBehaviour(
            //     // 1) Make sure the chart can scroll
            //     isScrollable: true,
            //   ),
            backgroundDecorations: [
              HorizontalAxisDecoration(
                endWithChart: false,
                lineWidth: 2.0,
                axisStep: 2,
                lineColor: Theme.of(context)
                    .colorScheme
                    .primaryVariant
                    .withOpacity(0.2),
              ),
              VerticalAxisDecoration(
                endWithChart: false,
                lineWidth: 2.0,
                axisStep: 7,
                lineColor: Theme.of(context)
                    .colorScheme
                    .primaryVariant
                    .withOpacity(0.8),
              ),
              GridDecoration(
                endWithChart: false,
                showVerticalGrid: true,
                showHorizontalValues: false,
                showVerticalValues: true,
                verticalValuesPadding:
                    const EdgeInsets.symmetric(vertical: 12.0),
                verticalAxisStep: 1,
                horizontalAxisStep: 1,
                textStyle: Theme.of(context).textTheme.caption,
                gridColor: Theme.of(context)
                    .colorScheme
                    .primaryVariant
                    .withOpacity(0.2),
              ),
            ]),
      ),
    );
  }

  Widget _viewMeses(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 35),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              CircleAvatar(child: Text('Ene')),
              CircleAvatar(child: Text('Feb')),
              CircleAvatar(child: Text('Mar')),
              CircleAvatar(child: Text('Abr'))
            ],
          ),
          const SizedBox(height: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              CircleAvatar(child: Text('May')),
              CircleAvatar(child: Text('Jun')),
              CircleAvatar(child: Text('Jul')),
              CircleAvatar(child: Text('Ago'))
            ],
          ),
          const SizedBox(height: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              CircleAvatar(child: Text('Sep')),
              CircleAvatar(child: Text('Oct')),
              CircleAvatar(child: Text('Nov')),
              CircleAvatar(child: Text('Dic'))
            ],
          ),
        ],
      ),
    );
  }
}
