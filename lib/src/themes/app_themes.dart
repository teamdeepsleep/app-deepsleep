import 'package:flutter/material.dart';

class AppTheme {
  static const Color primary = Colors.cyan;
  // static const Color acent = Color(0xFF4081);

  static const textStyle = TextStyle(
    fontSize: 18.0,
    color: Colors.blueGrey,
    letterSpacing: 2.0,
    fontWeight: FontWeight.bold,
  );

  static const textStyle2 = TextStyle(
      fontSize: 18.0,
      color: Colors.black45,
      letterSpacing: 2.0,
      fontWeight: FontWeight.w300);
    
  static final ThemeData lightTheme = ThemeData.light().copyWith(
      //color primario
      primaryColor: primary,

      //AppBar
      appBarTheme: const AppBarTheme(
        color: primary,
        elevation: 0,
        centerTitle: true,
      ),
      //Scaffold
      // scaffoldBackgroundColor: Colors.amber,
      //button
      textButtonTheme: TextButtonThemeData(
        style: TextButton.styleFrom(primary: primary),
      ),

      //sds
      bottomNavigationBarTheme: BottomNavigationBarThemeData(
        selectedItemColor: Colors.white,
        backgroundColor: primary,
        unselectedItemColor: Colors.cyan[900],
      ),
      //ElevatedButton
      elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
        primary: primary,
        shape: const StadiumBorder(),
        elevation: 5,
      )));
}
