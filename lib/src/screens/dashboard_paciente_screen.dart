import 'package:app_deepsleep/src/controllers/dashboard_paciente_controller.dart';
import 'package:app_deepsleep/src/screens/dashboard_paciente_tab1.dart';
import 'package:app_deepsleep/src/screens/dashboard_paciente_tab2.dart';
import 'package:app_deepsleep/src/screens/dashboard_paciente_tab3.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DashboardPacienteScreen extends StatelessWidget {
  const DashboardPacienteScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //   DashboardPacienteController
    Get.put(DashboardPacienteController());

    return GetBuilder<DashboardPacienteController>(builder: (controller) {
      return DefaultTabController(
          length: 3,
          child: Scaffold(
            appBar: AppBar(
              elevation: 2,
              flexibleSpace: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: const [
                  TabBar(tabs: [
                    Tab(text: 'Semana'),
                    Tab(text: 'Mes'),
                    Tab(text: 'PSQI'),
                  ])
                ],
              ),
            ),
            body: const TabBarView(physics: ScrollPhysics(), children: [
              DashboardPacienteTab1Screen(),
              DashboardPacienteTab2Screen(),
              DashboardPacienteTab3Screen(),
            ]),
          ));
    });
  }
}
