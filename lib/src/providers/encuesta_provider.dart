import 'package:app_deepsleep/src/environment/index.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class EncuestaProvider extends GetConnect {
  static const apiUrl = Environment.baseUrl;
  @override
  void onInit() {}

  Future enviarEncuesta(encuesta) async {
    const url = '$apiUrl/encuesta';
    print('url $url');
    print('encuestbb $encuesta');
    var data = {
      "pregunta1": encuesta["pregunta1"],
      "pregunta2": encuesta["pregunta2"],
      "pregunta3": encuesta["pregunta3"],
      "pregunta4": encuesta["pregunta4"],
      "pregunta5A": encuesta["pregunta5A"],
      "pregunta5B": encuesta["pregunta5B"],
      "pregunta5C": encuesta["pregunta5C"],
      "pregunta5D": encuesta["pregunta5D"],
      "pregunta5E": encuesta["pregunta5E"],
      "pregunta5F": encuesta["pregunta5F"],
      "pregunta5G": encuesta["pregunta5G"],
      "pregunta5H": encuesta["pregunta5H"],
      "pregunta5I": encuesta["pregunta5I"],
      "pregunta6": encuesta["pregunta6"],
      "pregunta7": encuesta["pregunta7"],
      "pregunta8": encuesta["pregunta8"],
      "pregunta9": encuesta["pregunta9"],
      "resultado": encuesta["resultado"],
      "idPaciente": encuesta["idPaciente"]
    };
    print('encuestaa $data');
    var res = await post(url, data);
    print(res);
    return res.statusCode;
  }
}
