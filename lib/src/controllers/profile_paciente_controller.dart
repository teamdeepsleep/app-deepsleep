import 'package:app_deepsleep/src/models/paciente_model.dart';
import 'package:app_deepsleep/src/models/response/paciente_response.dart';
import 'package:app_deepsleep/src/providers/providers.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class ProfilePacienteController extends GetxController {
  final profileProvider = Get.find<ProfileProvider>();
  Paciente pacienteEncontrado = Paciente();
  TextEditingController telefono = TextEditingController();

  ProfilePacienteController() {
    buscarProfilePaciente();
  }

  Future buscarProfilePaciente() async {
    var storage = GetStorage();
    var idUsuario = await storage.read('idUsuario');
    PacienteResponse pacientePorId =
        await profileProvider.buscarProfilePaciente(idUsuario);

    pacienteEncontrado.nombre = pacientePorId.nombre;
    pacienteEncontrado.apellidos = pacientePorId.apellido;
    pacienteEncontrado.dni = pacientePorId.dni;
    pacienteEncontrado.correo = pacientePorId.correo;
    pacienteEncontrado.imagen = pacientePorId.imagen;
    pacienteEncontrado.telefono = pacientePorId.telefono;
    print('buscarProfilePaciente');
    update();
  }

  Future editarPaciente() async {
    var storage = GetStorage();
    var idUsuario = await storage.read('idUsuario');
    var tel = telefono.value.text;
    print('el telefono enviado es $tel');
    if (tel.isEmpty) {
      Get.snackbar('validación', 'ingrese su número de celular');
      return;
    }
    if (tel.length != 9) {
      Get.snackbar('validación', 'ingresar un celular válido');
      return;
    }

    var respuesta = await profileProvider.editarPaciente(idUsuario, tel);
    if (respuesta.statusCode == 200) {
      Get.snackbar(
          'actualización', 'los datos fueron modificados correctamente');
      //   pacienteEncontrado.telefono = pacienteEncontrado.telefono;
      buscarProfilePaciente();
      //   update();
    } else {
      Get.snackbar('actualización', 'los datos no fueron modificados"');
    }
    print(respuesta);
  }
}
