import 'package:app_deepsleep/src/controllers/goal_controller.dart';
import 'package:app_deepsleep/src/providers/providers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class QuizController extends GetxController {
  final encuestaProvider = Get.find<EncuestaProvider>();
  var showRespuesta1 = false;
  var respuesta1 = TimeOfDay.now();
  var showRespuesta2 = false;
  TextEditingController respuesta2 = TextEditingController();
  var showRespuesta3 = false;
  var respuesta3 = TimeOfDay.now();
  var showRespuesta4 = false;
  TextEditingController respuesta4 = TextEditingController();
  var respuesta5a = 0;
  bool opcion5a1 = false;
  bool opcion5a2 = false;
  bool opcion5a3 = false;
  bool opcion5a4 = false;
  var respuesta5b = 0;
  bool opcion5b1 = false;
  bool opcion5b2 = false;
  bool opcion5b3 = false;
  bool opcion5b4 = false;
  var respuesta5c = 0;
  bool opcion5c1 = false;
  bool opcion5c2 = false;
  bool opcion5c3 = false;
  bool opcion5c4 = false;
  var respuesta5d = 0;
  bool opcion5d1 = false;
  bool opcion5d2 = false;
  bool opcion5d3 = false;
  bool opcion5d4 = false;
  var respuesta5e = 0;
  bool opcion5e1 = false;
  bool opcion5e2 = false;
  bool opcion5e3 = false;
  bool opcion5e4 = false;

  var respuesta5f = 0;
  bool opcion5f1 = false;
  bool opcion5f2 = false;
  bool opcion5f3 = false;
  bool opcion5f4 = false;

  var respuesta5g = 0;
  bool opcion5g1 = false;
  bool opcion5g2 = false;
  bool opcion5g3 = false;
  bool opcion5g4 = false;

  var respuesta5h = 0;
  bool opcion5h1 = false;
  bool opcion5h2 = false;
  bool opcion5h3 = false;
  bool opcion5h4 = false;

  var respuesta5i = 0;
  bool opcion5i1 = false;
  bool opcion5i2 = false;
  bool opcion5i3 = false;
  bool opcion5i4 = false;

  var respuesta6 = 0;
  bool opcion61 = false;
  bool opcion62 = false;
  bool opcion63 = false;
  bool opcion64 = false;

  var respuesta7 = 0;
  bool opcion71 = false;
  bool opcion72 = false;
  bool opcion73 = false;
  bool opcion74 = false;

  var respuesta8 = 0;
  bool opcion81 = false;
  bool opcion82 = false;
  bool opcion83 = false;
  bool opcion84 = false;

  var respuesta9 = 0;
  bool opcion91 = false;
  bool opcion92 = false;
  bool opcion93 = false;
  bool opcion94 = false;

  QuizController() {}

  setRespuesta5a(int opcion, bool isSelect) {
    if (isSelect == true) {
      respuesta5a = opcion;
      switch (opcion) {
        case 1:
          opcion5a1 = true;
          opcion5a2 = false;
          opcion5a3 = false;
          opcion5a4 = false;
          break;
        case 2:
          opcion5a1 = false;
          opcion5a2 = true;
          opcion5a3 = false;
          opcion5a4 = false;
          break;
        case 3:
          opcion5a1 = false;
          opcion5a2 = false;
          opcion5a3 = true;
          opcion5a4 = false;
          break;
        case 4:
          opcion5a1 = false;
          opcion5a2 = false;
          opcion5a3 = false;
          opcion5a4 = true;
          break;
        default:
      }
    } else {
      opcion5a1 = false;
      opcion5a2 = false;
      opcion5a3 = false;
      opcion5a4 = false;
      respuesta5a = 0;
    }

    update();
  }

  setRespuesta5b(int opcion, bool isSelect) {
    if (isSelect == true) {
      respuesta5b = opcion;
      switch (opcion) {
        case 1:
          opcion5b1 = true;
          opcion5b2 = false;
          opcion5b3 = false;
          opcion5b4 = false;
          break;
        case 2:
          opcion5b1 = false;
          opcion5b2 = true;
          opcion5b3 = false;
          opcion5b4 = false;
          break;
        case 3:
          opcion5b1 = false;
          opcion5b2 = false;
          opcion5b3 = true;
          opcion5b4 = false;
          break;
        case 4:
          opcion5b1 = false;
          opcion5b2 = false;
          opcion5b3 = false;
          opcion5b4 = true;
          break;
        default:
      }
    } else {
      opcion5b1 = false;
      opcion5b2 = false;
      opcion5b3 = false;
      opcion5b4 = false;
      respuesta5b = 0;
    }

    update();
  }

  setRespuesta5c(int opcion, bool isSelect) {
    if (isSelect == true) {
      respuesta5c = opcion;
      switch (opcion) {
        case 1:
          opcion5c1 = true;
          opcion5c2 = false;
          opcion5c3 = false;
          opcion5c4 = false;
          break;
        case 2:
          opcion5c1 = false;
          opcion5c2 = true;
          opcion5c3 = false;
          opcion5c4 = false;
          break;
        case 3:
          opcion5c1 = false;
          opcion5c2 = false;
          opcion5c3 = true;
          opcion5c4 = false;
          break;
        case 4:
          opcion5c1 = false;
          opcion5c2 = false;
          opcion5c3 = false;
          opcion5c4 = true;
          break;
        default:
      }
    } else {
      opcion5c1 = false;
      opcion5c2 = false;
      opcion5c3 = false;
      opcion5c4 = false;
      respuesta5c = 0;
    }

    update();
  }

  setRespuesta5d(int opcion, bool isSelect) {
    if (isSelect == true) {
      respuesta5d = opcion;
      switch (opcion) {
        case 1:
          opcion5d1 = true;
          opcion5d2 = false;
          opcion5d3 = false;
          opcion5d4 = false;
          break;
        case 2:
          opcion5d1 = false;
          opcion5d2 = true;
          opcion5d3 = false;
          opcion5d4 = false;
          break;
        case 3:
          opcion5d1 = false;
          opcion5d2 = false;
          opcion5d3 = true;
          opcion5d4 = false;
          break;
        case 4:
          opcion5d1 = false;
          opcion5d2 = false;
          opcion5d3 = false;
          opcion5d4 = true;
          break;
        default:
      }
    } else {
      opcion5d1 = false;
      opcion5d2 = false;
      opcion5d3 = false;
      opcion5d4 = false;
      respuesta5d = 0;
    }

    update();
  }

  setRespuesta5e(int opcion, bool isSelect) {
    if (isSelect == true) {
      respuesta5e = opcion;
      switch (opcion) {
        case 1:
          opcion5e1 = true;
          opcion5e2 = false;
          opcion5e3 = false;
          opcion5e4 = false;
          break;
        case 2:
          opcion5e1 = false;
          opcion5e2 = true;
          opcion5e3 = false;
          opcion5e4 = false;
          break;
        case 3:
          opcion5e1 = false;
          opcion5e2 = false;
          opcion5e3 = true;
          opcion5e4 = false;
          break;
        case 4:
          opcion5e1 = false;
          opcion5e2 = false;
          opcion5e3 = false;
          opcion5e4 = true;
          break;
        default:
      }
    } else {
      opcion5e1 = false;
      opcion5e2 = false;
      opcion5e3 = false;
      opcion5e4 = false;
      respuesta5e = 0;
    }

    update();
  }

  setRespuesta5f(int opcion, bool isSelect) {
    if (isSelect == true) {
      respuesta5f = opcion;
      switch (opcion) {
        case 1:
          opcion5f1 = true;
          opcion5f2 = false;
          opcion5f3 = false;
          opcion5f4 = false;
          break;
        case 2:
          opcion5f1 = false;
          opcion5f2 = true;
          opcion5f3 = false;
          opcion5f4 = false;
          break;
        case 3:
          opcion5f1 = false;
          opcion5f2 = false;
          opcion5f3 = true;
          opcion5f4 = false;
          break;
        case 4:
          opcion5f1 = false;
          opcion5f2 = false;
          opcion5f3 = false;
          opcion5f4 = true;
          break;
        default:
      }
    } else {
      opcion5f1 = false;
      opcion5f2 = false;
      opcion5f3 = false;
      opcion5f4 = false;
      respuesta5f = 0;
    }

    update();
  }

  setRespuesta5g(int opcion, bool isSelect) {
    if (isSelect == true) {
      respuesta5g = opcion;
      switch (opcion) {
        case 1:
          opcion5g1 = true;
          opcion5g2 = false;
          opcion5g3 = false;
          opcion5g4 = false;
          break;
        case 2:
          opcion5g1 = false;
          opcion5g2 = true;
          opcion5g3 = false;
          opcion5g4 = false;
          break;
        case 3:
          opcion5g1 = false;
          opcion5g2 = false;
          opcion5g3 = true;
          opcion5g4 = false;
          break;
        case 4:
          opcion5g1 = false;
          opcion5g2 = false;
          opcion5g3 = false;
          opcion5g4 = true;
          break;
        default:
      }
    } else {
      opcion5g1 = false;
      opcion5g2 = false;
      opcion5g3 = false;
      opcion5g4 = false;
      respuesta5g = 0;
    }

    update();
  }

  setRespuesta5h(int opcion, bool isSelect) {
    if (isSelect == true) {
      respuesta5h = opcion;
      switch (opcion) {
        case 1:
          opcion5h1 = true;
          opcion5h2 = false;
          opcion5h3 = false;
          opcion5h4 = false;
          break;
        case 2:
          opcion5h1 = false;
          opcion5h2 = true;
          opcion5h3 = false;
          opcion5h4 = false;
          break;
        case 3:
          opcion5h1 = false;
          opcion5h2 = false;
          opcion5h3 = true;
          opcion5h4 = false;
          break;
        case 4:
          opcion5h1 = false;
          opcion5h2 = false;
          opcion5h3 = false;
          opcion5h4 = true;
          break;
        default:
      }
    } else {
      opcion5h1 = false;
      opcion5h2 = false;
      opcion5h3 = false;
      opcion5h4 = false;
      respuesta5h = 0;
    }

    update();
  }

  setRespuesta5i(int opcion, bool isSelect) {
    if (isSelect == true) {
      respuesta5i = opcion;
      switch (opcion) {
        case 1:
          opcion5i1 = true;
          opcion5i2 = false;
          opcion5i3 = false;
          opcion5i4 = false;
          break;
        case 2:
          opcion5i1 = false;
          opcion5i2 = true;
          opcion5i3 = false;
          opcion5i4 = false;
          break;
        case 3:
          opcion5i1 = false;
          opcion5i2 = false;
          opcion5i3 = true;
          opcion5i4 = false;
          break;
        case 4:
          opcion5i1 = false;
          opcion5i2 = false;
          opcion5i3 = false;
          opcion5i4 = true;
          break;
        default:
      }
    } else {
      opcion5i1 = false;
      opcion5i2 = false;
      opcion5i3 = false;
      opcion5i4 = false;
      respuesta5i = 0;
    }

    update();
  }

  setRespuesta6(int opcion, bool isSelect) {
    if (isSelect == true) {
      respuesta6 = opcion;
      switch (opcion) {
        case 1:
          opcion61 = true;
          opcion62 = false;
          opcion63 = false;
          opcion64 = false;
          break;
        case 2:
          opcion61 = false;
          opcion62 = true;
          opcion63 = false;
          opcion64 = false;
          break;
        case 3:
          opcion61 = false;
          opcion62 = false;
          opcion63 = true;
          opcion64 = false;
          break;
        case 4:
          opcion61 = false;
          opcion62 = false;
          opcion63 = false;
          opcion64 = true;
          break;
        default:
      }
    } else {
      opcion61 = false;
      opcion62 = false;
      opcion63 = false;
      opcion64 = false;
      respuesta6 = 0;
    }

    update();
  }

  setRespuesta7(int opcion, bool isSelect) {
    if (isSelect == true) {
      respuesta7 = opcion;
      switch (opcion) {
        case 1:
          opcion71 = true;
          opcion72 = false;
          opcion73 = false;
          opcion74 = false;
          break;
        case 2:
          opcion71 = false;
          opcion72 = true;
          opcion73 = false;
          opcion74 = false;
          break;
        case 3:
          opcion71 = false;
          opcion72 = false;
          opcion73 = true;
          opcion74 = false;
          break;
        case 4:
          opcion71 = false;
          opcion72 = false;
          opcion73 = false;
          opcion74 = true;
          break;
        default:
      }
    } else {
      opcion71 = false;
      opcion72 = false;
      opcion73 = false;
      opcion74 = false;
      respuesta7 = 0;
    }

    update();
  }

  setRespuesta8(int opcion, bool isSelect) {
    if (isSelect == true) {
      respuesta8 = opcion;
      switch (opcion) {
        case 1:
          opcion81 = true;
          opcion82 = false;
          opcion83 = false;
          opcion84 = false;
          break;
        case 2:
          opcion81 = false;
          opcion82 = true;
          opcion83 = false;
          opcion84 = false;
          break;
        case 3:
          opcion81 = false;
          opcion82 = false;
          opcion83 = true;
          opcion84 = false;
          break;
        case 4:
          opcion81 = false;
          opcion82 = false;
          opcion83 = false;
          opcion84 = true;
          break;
        default:
      }
    } else {
      opcion81 = false;
      opcion82 = false;
      opcion83 = false;
      opcion84 = false;
      respuesta8 = 0;
    }

    update();
  }

  setRespuesta9(int opcion, bool isSelect) {
    if (isSelect == true) {
      respuesta9 = opcion;
      switch (opcion) {
        case 1:
          opcion91 = true;
          opcion92 = false;
          opcion93 = false;
          opcion94 = false;
          break;
        case 2:
          opcion91 = false;
          opcion92 = true;
          opcion93 = false;
          opcion94 = false;
          break;
        case 3:
          opcion91 = false;
          opcion92 = false;
          opcion93 = true;
          opcion94 = false;
          break;
        case 4:
          opcion91 = false;
          opcion92 = false;
          opcion93 = false;
          opcion94 = true;
          break;
        default:
      }
    } else {
      opcion91 = false;
      opcion92 = false;
      opcion93 = false;
      opcion94 = false;
      respuesta9 = 0;
    }

    update();
  }

  Future<int> enviarEncuesta() async {
    //validar Encuesta
    if (showRespuesta1 == false ||
        showRespuesta2 == false ||
        showRespuesta3 == false ||
        showRespuesta4 == false ||
        respuesta5a == 0 ||
        respuesta5b == 0 ||
        respuesta5c == 0 ||
        respuesta5d == 0 ||
        respuesta5e == 0 ||
        respuesta5f == 0 ||
        respuesta5g == 0 ||
        respuesta5h == 0 ||
        respuesta5i == 0 ||
        respuesta6 == 0 ||
        respuesta7 == 0 ||
        respuesta8 == 0 ||
        respuesta9 == 0) {
      Get.snackbar('encuesta', 'Completar todos los campos');

      return 0;
    }

    var resultado = calcularPSQI();
    var storage = GetStorage();
    var idUsuario = await storage.read('idUsuario');

    var body = {
      "pregunta1": 0, //respuesta1,
      "pregunta2": respuesta2.text,
      "pregunta3": 0, //respuesta3,
      "pregunta4": respuesta4.text,
      "pregunta5A": respuesta5a,
      "pregunta5B": respuesta5b,
      "pregunta5C": respuesta5c,
      "pregunta5D": respuesta5d,
      "pregunta5E": respuesta5e,
      "pregunta5F": respuesta5f,
      "pregunta5G": respuesta5g,
      "pregunta5H": respuesta5h,
      "pregunta5I": respuesta5i,
      "pregunta6": respuesta6,
      "pregunta7": respuesta7,
      "pregunta8": respuesta8,
      "pregunta9": respuesta9,
      "resultado": resultado,
      "idPaciente": idUsuario
    };
    Get.snackbar('encuesta', 'resultado es $resultado');
    var resEncuesta = await encuestaProvider.enviarEncuesta(body);

    if (resEncuesta == 200) {
      Get.snackbar('encuesta', 'se realizo la encuesta correctamente');
      var goalController = Get.put(GoalController());
      goalController.buscarPSQI();
      limpiarCampos();
      return 1;
    } else {
      Get.snackbar('encuesta', 'no se realizo la encuesta correctamente');
      return 2;
    }
  }

  limpiarCampos() {
    showRespuesta1 = false;
    respuesta1 = TimeOfDay.now();
    showRespuesta2 = false;
    respuesta2.text = '';
    showRespuesta3 = false;
    respuesta3 = TimeOfDay.now();
    showRespuesta4 = false;
    respuesta4.text = '';

    respuesta5a = 0;
    opcion5a1 = false;
    opcion5a2 = false;
    opcion5a3 = false;
    opcion5a4 = false;
    respuesta5b = 0;
    opcion5b1 = false;
    opcion5b2 = false;
    opcion5b3 = false;
    opcion5b4 = false;
    respuesta5c = 0;
    opcion5c1 = false;
    opcion5c2 = false;
    opcion5c3 = false;
    opcion5c4 = false;
    respuesta5d = 0;
    opcion5d1 = false;
    opcion5d2 = false;
    opcion5d3 = false;
    opcion5d4 = false;
    respuesta5e = 0;
    opcion5e1 = false;
    opcion5e2 = false;
    opcion5e3 = false;
    opcion5e4 = false;

    respuesta5f = 0;
    opcion5f1 = false;
    opcion5f2 = false;
    opcion5f3 = false;
    opcion5f4 = false;

    respuesta5g = 0;
    opcion5g1 = false;
    opcion5g2 = false;
    opcion5g3 = false;
    opcion5g4 = false;

    respuesta5h = 0;
    opcion5h1 = false;
    opcion5h2 = false;
    opcion5h3 = false;
    opcion5h4 = false;

    respuesta5i = 0;
    opcion5i1 = false;
    opcion5i2 = false;
    opcion5i3 = false;
    opcion5i4 = false;

    respuesta6 = 0;
    opcion61 = false;
    opcion62 = false;
    opcion63 = false;
    opcion64 = false;

    respuesta7 = 0;
    opcion71 = false;
    opcion72 = false;
    opcion73 = false;
    opcion74 = false;

    respuesta8 = 0;
    opcion81 = false;
    opcion82 = false;
    opcion83 = false;
    opcion84 = false;

    respuesta9 = 0;
    opcion91 = false;
    opcion92 = false;
    opcion93 = false;
    opcion94 = false;
    update();
  }

  int calcularPSQI() {
    var psqi = 0;

    //componente 1
    int com1 = componente1();
    print('comp1 $com1');
    //componente 2
    int com2 = componente2();
    print('comp2 $com2');
    //componente 3
    int com3 = componente3();
    print('comp3 $com3');
    //componente 4
    int com4 = componente4();
    print('comp4 $com4');
    //componente 5
    int com5 = componente5();
    print('comp5 $com5');
    //componente 6
    int com6 = componente6();
    print('comp6 $com6');
    //componente 7
    int com7 = componente7();
    print('comp7 $com7');
    psqi = com1 + com2 + com3 + com4 + com5 + com6 + com7;
    return psqi;
  }

  int componente1() {
    var score = 0;

    switch (respuesta6) {
      case 1:
        score = score + 0;
        break;
      case 2:
        score = score + 1;
        break;
      case 3:
        score = score + 2;
        break;
      case 4:
        score = score + 3;
        break;
      default:
    }
    return score;
  }

  int componente2() {
    var score = 0;
    int r2 = int.parse(respuesta2.text);

    if (r2 <= 15) {
      score = score + 0;
    } else if (16 <= r2 && r2 <= 30) {
      score = score + 1;
    } else if (31 <= r2 && r2 <= 60) {
      score = score + 2;
    } else {
      score = score + 3;
    }

    switch (respuesta5a) {
      case 1:
        score = score + 0;
        break;
      case 2:
        score = score + 1;
        break;
      case 3:
        score = score + 2;
        break;
      case 4:
        score = score + 3;
        break;
      default:
    }

    if (score <= 0) {
      return 0;
    } else if (1 <= score && score <= 2) {
      return 1;
    } else if (3 <= score && score <= 4) {
      return 2;
    } else {
      return 3;
    }
    // return score;
  }

  int componente3() {
    var score = 0;
    int r4 = int.parse(respuesta4.text);

    if (r4 < 5) {
      score = score + 3;
    } else if (5 <= r4 && r4 <= 6) {
      score = score + 2;
    } else if (6 < r4 && r4 <= 7) {
      score = score + 1;
    } else {
      score = score + 0;
    }

    return score;
  }

  int componente4() {
    int score = 0;
    double x = 0.0;
    int timeDespertar = (respuesta3.hour * 60) + respuesta3.minute;
    int timeDormir = (respuesta1.hour * 60) + respuesta1.minute;

    if (timeDespertar - timeDormir > 0) {
      int dif = timeDespertar - timeDormir;
      int h = (dif / 60).toInt();
      x = (int.parse(respuesta4.text) / h) * 100;
    } else {
      int dif = timeDespertar + (1440 - timeDormir);
      int h = (dif / 60).toInt();
      x = (int.parse(respuesta4.text) / h) * 100;
    }

    if (x < 65) {
      return 3;
    } else if (65 < x && x < 74) {
      return 2;
    } else if (75 < x && x < 84) {
      return 1;
    } else {
      return 0;
    }
  }

  int componente5() {
    int score = 0;
    switch (respuesta5b) {
      case 1:
        score = score + 0;
        break;
      case 2:
        score = score + 1;
        break;
      case 3:
        score = score + 2;
        break;
      case 4:
        score = score + 3;
        break;
      default:
    }

    switch (respuesta5c) {
      case 1:
        score = score + 0;
        break;
      case 2:
        score = score + 1;
        break;
      case 3:
        score = score + 2;
        break;
      case 4:
        score = score + 3;
        break;
      default:
    }

    switch (respuesta5d) {
      case 1:
        score = score + 0;
        break;
      case 2:
        score = score + 1;
        break;
      case 3:
        score = score + 2;
        break;
      case 4:
        score = score + 3;
        break;
      default:
    }

    switch (respuesta5e) {
      case 1:
        score = score + 0;
        break;
      case 2:
        score = score + 1;
        break;
      case 3:
        score = score + 2;
        break;
      case 4:
        score = score + 3;
        break;
      default:
    }

    switch (respuesta5f) {
      case 1:
        score = score + 0;
        break;
      case 2:
        score = score + 1;
        break;
      case 3:
        score = score + 2;
        break;
      case 4:
        score = score + 3;
        break;
      default:
    }

    switch (respuesta5g) {
      case 1:
        score = score + 0;
        break;
      case 2:
        score = score + 1;
        break;
      case 3:
        score = score + 2;
        break;
      case 4:
        score = score + 3;
        break;
      default:
    }

    switch (respuesta5h) {
      case 1:
        score = score + 0;
        break;
      case 2:
        score = score + 1;
        break;
      case 3:
        score = score + 2;
        break;
      case 4:
        score = score + 3;
        break;
      default:
    }

    switch (respuesta5i) {
      case 1:
        score = score + 0;
        break;
      case 2:
        score = score + 1;
        break;
      case 3:
        score = score + 2;
        break;
      case 4:
        score = score + 3;
        break;
      default:
    }

    if (score <= 0) {
      return 0;
    } else if (1 <= score && score <= 9) {
      return 1;
    } else if (10 <= score && score <= 18) {
      return 2;
    } else {
      return 3;
    }
  }

  int componente6() {
    int score = 0;

    switch (respuesta7) {
      case 1:
        score = score + 0;
        break;
      case 2:
        score = score + 1;
        break;
      case 3:
        score = score + 2;
        break;
      case 4:
        score = score + 3;
        break;
      default:
    }

    return score;
  }

  int componente7() {
    int score = 0;

    switch (respuesta8) {
      case 1:
        score = score + 0;
        break;
      case 2:
        score = score + 1;
        break;
      case 3:
        score = score + 2;
        break;
      case 4:
        score = score + 3;
        break;
      default:
    }

    switch (respuesta9) {
      case 1:
        score = score + 0;
        break;
      case 2:
        score = score + 1;
        break;
      case 3:
        score = score + 2;
        break;
      case 4:
        score = score + 3;
        break;
      default:
    }

    if (score <= 0) {
      return 0;
    } else if (1 <= score && score <= 2) {
      return 1;
    } else if (3 <= score && score <= 4) {
      return 2;
    } else {
      return 3;
    }
  }

  validarDatos() {
    if (respuesta5a == 0 ||
        respuesta5b == 0 ||
        respuesta5c == 0 ||
        respuesta5d == 0 ||
        respuesta5e == 0 ||
        respuesta5f == 0 ||
        respuesta5g == 0 ||
        respuesta5h == 0 ||
        respuesta5i == 0 ||
        respuesta6 == 0 ||
        respuesta7 == 0 ||
        respuesta8 == 0 ||
        respuesta9 == 0) {
      Get.snackbar('encuesta', 'Completar datos');
    }
  }

  changePregunta(int pregunta, TimeOfDay time) {
    if (pregunta == 1) {
      respuesta1 = time;
      showRespuesta1 = true;
      update();
    }
    if (pregunta == 2) {
      //   respuesta2 = ;
      showRespuesta2 = true;
      update();
    }
    if (pregunta == 3) {
      respuesta3 = time;
      showRespuesta3 = true;
      update();
    }
    if (pregunta == 4) {
      showRespuesta4 = true;
      update();
    }
  }
}
