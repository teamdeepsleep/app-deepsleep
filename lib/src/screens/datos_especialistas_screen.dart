import 'package:app_deepsleep/src/controllers/datos_especialista_controller.dart';
import 'package:app_deepsleep/src/models/response/detalle_especialista_response.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DatosEspecialistaScreen extends StatelessWidget {
  const DatosEspecialistaScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(DatosEspecialistasController());

    return GetBuilder<DatosEspecialistasController>(builder: (controller) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Datos'),
        ),
        body: FutureBuilder(
          future: controller.datosDetalleEspecialista(),
          builder: (BuildContext context,
              AsyncSnapshot<ResponseDetalleEspecialista> snapshot) {
            if (snapshot.data == null) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }

            if (snapshot.connectionState == ConnectionState.done) {
              ResponseDetalleEspecialista? datos = snapshot.data;
              if (datos == null) {
                return const Text('----');
              } else {
                if (datos.operacion == false) {
                  return Text(datos.mensaje);
                } else {
                  return Column(
                    children: [
                      Card(
                        child: ListTile(
                          title: Text(
                              '${datos.data.nombre} ${datos.data.apellido}'),
                          subtitle: const Text('Nombre'),
                          //   trailing: const Icon(Icons.edit),
                        ),
                      ),
                      // Divider(),
                      Card(
                        child: ListTile(
                          title: Text(datos.data.correo),
                          subtitle: const Text('Correo'),
                        ),
                      ),
                      Card(
                        child: ListTile(
                          title: Text(datos.data.dni),
                          subtitle: const Text('Dni'),
                        ),
                      ),
                      Card(
                        child: ListTile(
                          title: Text(datos.data.telefono),
                          subtitle: const Text('Telefono'),
                          trailing: InkWell(
                              onTap: () async {
                                _editarCampo(context, controller, 'telefono');
                              },
                              child: Icon(Icons.edit)),
                        ),
                      ),
                      Card(
                        child: ListTile(
                          title: Text(datos.data.trabajo),
                          subtitle: const Text('Centro Laboral'),
                          trailing: InkWell(
                              onTap: () async {
                                _editarCampo(context, controller, 'trabajo');
                              },
                              child: const Icon(Icons.edit)),
                        ),
                      ),
                      const Text('Especialidades'),
                      Expanded(
                          child: ListView.builder(
                              // scrollDirection: Axis.vertical,
                              // shrinkWrap: false,
                              itemCount: datos.data.especialidades.length,
                              itemBuilder: (context, index) {
                                return ListTile(
                                  title: Text(datos.data.especialidades[index]),
                                  trailing: InkWell(
                                      onTap: () async {
                                        _showEliminarEspecialidad(
                                            context, controller, index);
                                      },
                                      child: const Icon(Icons.delete)),
                                );
                              }))
                    ],
                  );
                }
              }
            }
            return const Text('Default vista2');
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            _showAgregarEspecialidad(context, controller);
          },
          child: const Icon(Icons.add_task),
        ),
      );
    });
  }
}

void _showAgregarEspecialidad(
    BuildContext context, DatosEspecialistasController controller) {
  showDialog(
      barrierDismissible: false,
      context: context,
      builder: (_) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadiusDirectional.circular(15)),
          elevation: 5,
          title: const Text('Nueva Especialidad'),
          content: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                TextField(
                  controller: controller.nuevaEspecialidad,
                  keyboardType: TextInputType.text,
                  decoration: const InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    border: OutlineInputBorder(),
                    label: Text('Especialidad'),
                  ),
                )
              ],
            ),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('Cancelar'),
            ),
            TextButton(
              onPressed: () async {
                var respuesta = await controller.agregarEspecialidad();
                Navigator.pop(context);
              },
              child: const Text('Ok'),
            )
          ],
        );
      });
}

void _showEliminarEspecialidad(
    BuildContext context, DatosEspecialistasController controller, int index) {
  showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext _) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadiusDirectional.circular(15)),
          elevation: 5,
          title: const Text('Eliminar Especialidad'),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('Cancelar'),
            ),
            TextButton(
              onPressed: () async {
                var respuesta = await controller.eliminarEspecialidad(index);
                Navigator.pop(context);
              },
              child: const Text('Ok'),
            )
          ],
        );
      });
}

void _editarCampo(BuildContext context, DatosEspecialistasController controller,
    String campo) {
  showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext _) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadiusDirectional.circular(15)),
          elevation: 5,
          title: const Text('Actualizar Especialista'),
          content: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                (campo == 'telefono')
                    ? TextField(
                        controller: controller.telefono,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          border: const OutlineInputBorder(),
                          label: Text(campo),
                        ),
                      )
                    : Container(),
                (campo == 'trabajo')
                    ? TextField(
                        controller: controller.centroLaboral,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          border: const OutlineInputBorder(),
                          label: Text(campo),
                        ),
                      )
                    : Container(),
              ],
            ),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('Cancelar'),
            ),
            TextButton(
              onPressed: () async {
                var respuesta = await controller.actualizarEspecialista(campo);
                Navigator.pop(context);
              },
              child: const Text('Ok'),
            )
          ],
        );
      });
}
