import 'package:app_deepsleep/src/models/recomendacion_model.dart';
import 'package:app_deepsleep/src/providers/providers.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class RecommendationController extends GetxController {
  late List<Recomendacion> misRecomedaciones = <Recomendacion>[];
  final habitoProvider = Get.find<RecomendacionProvider>();
  RecommendationController() {
    listarMisRecomendaciones();
  }

  Future listarMisRecomendaciones() async {
    var storage = GetStorage();
    var idUsuario = await storage.read('idUsuario');
    List<Recomendacion> lista =
        await habitoProvider.listarMisRecomendaciones(idUsuario);
    misRecomedaciones = [];
    misRecomedaciones.addAll(lista);
    update();
    print(misRecomedaciones.length);
    // final recomendacionesEncontradas =
    //     await habitoProvider.listarMisRecomendaciones(idUsuario);
    // if (recomendacionesEncontradas.isEmpty) {
    // } else {
    //   misRecomedaciones.addAll(recomendacionesEncontradas);
    //   update();
    // }
  }
}
