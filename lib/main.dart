import 'package:app_deepsleep/src/app.dart';
import 'package:app_deepsleep/src/providers/providers.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:get/get.dart';

void main() async {
  await GetStorage.init();
  Get.lazyPut(() => LoginProvider());
  Get.lazyPut(() => ProfileProvider());
  Get.lazyPut(() => HabitoProvider());
  Get.lazyPut(() => RecomendacionProvider());
  Get.lazyPut(() => EncuestaProvider());
  Get.lazyPut(() => EspecialistaProvider());
  runApp(const MyApp());
}
