import 'package:app_deepsleep/src/controllers/recommendation_controller.dart';
import 'package:app_deepsleep/src/models/recomendacion_model.dart';
import 'package:app_deepsleep/src/themes/app_themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RecommendationScreen extends StatelessWidget {
  const RecommendationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(RecommendationController());

    return GetBuilder<RecommendationController>(builder: (controller) {
      //   print(controller.misRecomedaciones.length);
      return (controller.misRecomedaciones.isEmpty)
          ? const Padding(
              padding: EdgeInsets.all(15.0),
              child: Center(
                child: Text(
                    'no hay recomendacioones personalizadas por el momento'),
              ),
            )
          : Scaffold(
              body: Center(
                child: ListView.builder(
                    itemCount: controller.misRecomedaciones.length,
                    itemBuilder: (context, index) {
                      return _item(
                          context, controller.misRecomedaciones[index]);
                    }),
              ),
            );
    });
  }
}

Widget _item(BuildContext context, Recomendacion recomendacion) {
  return InkWell(
    onTap: () async {
      Navigator.pushNamed(context, 'recommendation_detail',
          arguments: recomendacion);
    },
    child: Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      elevation: 15,
      shadowColor: AppTheme.primary.withOpacity(0.4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          FadeInImage(
            image: NetworkImage(recomendacion.imagen),
            placeholder: const AssetImage('assets/img/loading.gif'),
            width: 100,
            height: 110,
            fit: BoxFit.cover,
          ),
          Container(
            //   height: 120,
            // color: Colors.red,
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.spaceBe,

              children: [
                Container(
                    width: 200,
                    //   color: Colors.red,
                    child: Text(recomendacion.titulo)),
                Container(
                  // color: Colors.amber,
                  width: 200,
                  child:
                      Text('${recomendacion.descripcion.substring(0, 25)}...'),
                ),
              ],
            ),
          )
        ],
      ),
    ),
  );
}
