import 'package:app_deepsleep/src/controllers/perfil_especialista_controller.dart';
import 'package:app_deepsleep/src/controllers/profile_especialista_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

// https://i.pinimg.com/originals/10/f3/3a/10f33a90120d9cfe137ce22bfdb8d243.jpg
class ProfileEspecialistaScreen extends StatelessWidget {
  const ProfileEspecialistaScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(PerfilEspecialistaController());
    return GetBuilder<PerfilEspecialistaController>(builder: (controller) {
      return Scaffold(
        body: SafeArea(
            child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(
                            controller.especialistaEncontrado.imagen),
                        fit: BoxFit.cover)),
                child: SizedBox(
                  width: double.infinity,
                  height: 200,
                  child: Container(
                    alignment: const Alignment(0.0, 2.5),
                    child: CircleAvatar(
                      backgroundImage: NetworkImage(
                          controller.especialistaEncontrado.imagen),
                      radius: 60.0,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 60,
              ),
              Text(
                '${controller.especialistaEncontrado.nombre} ${controller.especialistaEncontrado.apellido}',
                style: const TextStyle(
                    fontSize: 25.0,
                    color: Colors.blueGrey,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.w400),
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                'Celular',
                style: TextStyle(
                    fontSize: 18.0,
                    color: Colors.black45,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.w300),
              ),
              Text(
                controller.especialistaEncontrado.telefono,
                style: const TextStyle(
                    fontSize: 18.0,
                    color: Colors.black45,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                'DNI',
                style: TextStyle(
                    fontSize: 18.0,
                    color: Colors.black45,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.w300),
              ),
              Text(
                controller.especialistaEncontrado.dni,
                style: const TextStyle(
                    fontSize: 18.0,
                    color: Colors.black45,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                'Correo',
                style: TextStyle(
                    fontSize: 18.0,
                    color: Colors.black45,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.w300),
              ),
              Text(
                controller.especialistaEncontrado.correo,
                style: const TextStyle(
                    fontSize: 15.0,
                    color: Colors.black45,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 120,
              ),
              Container(
                width: double.infinity,
                margin: const EdgeInsets.all(8),
                child: ElevatedButton(
                  onPressed: () async {
                    //   displayDialogAndroid(context, controller);
                    Navigator.pushNamed(context, 'especialista_datos');
                  },
                  child: const Text('DETALLE'),
                ),
              )
            ],
          ),
        )),
      );
    });
  }
}
