// To parse this JSON data, do
//
//     final registroPacienteResponse = registroPacienteResponseFromMap(jsonString);

import 'dart:convert';

class RegistroPacienteResponse {
  RegistroPacienteResponse({
    required this.operacion,
    required this.data,
  });

  final bool operacion;
  final PacienteRESPOST data;

  factory RegistroPacienteResponse.fromJson(String str) =>
      RegistroPacienteResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory RegistroPacienteResponse.fromMap(Map<String, dynamic> json) =>
      RegistroPacienteResponse(
        operacion: json["operacion"],
        data: PacienteRESPOST.fromMap(json["data"]),
      );

  Map<String, dynamic> toMap() => {
        "operacion": operacion,
        "data": data.toMap(),
      };
}

class PacienteRESPOST {
  PacienteRESPOST({
    required this.nombre,
    required this.apellido,
    required this.dni,
    required this.correo,
    required this.password,
    required this.habilitado,
    required this.psqi,
    required this.recomendaciones,
    required this.id,
    required this.v,
  });

  final String nombre;
  final String apellido;
  final String dni;
  final String correo;
  final String password;
  final bool habilitado;
  final int psqi;
  final List<dynamic> recomendaciones;
  final String id;
  final int v;

  factory PacienteRESPOST.fromJson(String str) =>
      PacienteRESPOST.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory PacienteRESPOST.fromMap(Map<String, dynamic> json) => PacienteRESPOST(
        nombre: json["nombre"],
        apellido: json["apellido"],
        dni: json["dni"],
        correo: json["correo"],
        password: json["password"],
        habilitado: json["habilitado"],
        psqi: json["psqi"] ?? '',
        recomendaciones:
            List<dynamic>.from(json["recomendaciones"].map((x) => x)),
        id: json["_id"],
        v: json["__v"],
      );

  Map<String, dynamic> toMap() => {
        "nombre": nombre,
        "apellido": apellido,
        "dni": dni,
        "correo": correo,
        "password": password,
        "habilitado": habilitado,
        "psqi": psqi,
        "recomendaciones": List<dynamic>.from(recomendaciones.map((x) => x)),
        "_id": id,
        "__v": v,
      };
}
