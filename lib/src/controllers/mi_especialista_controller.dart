import 'package:app_deepsleep/src/providers/especialista_provider.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class MiEspecialistaController extends GetxController {
  final especialistaProvider = Get.find<EspecialistaProvider>();
  MiEspecialistaController() {
    estadoMiEspecialista();
  }

  buscarMiEspecialista() {}

  Future<Map<String, dynamic>> estadoMiEspecialista() async {
    var storage = GetStorage();
    var idUsuario = await storage.read('idUsuario');
    Map<String, dynamic> sr =
        await especialistaProvider.miEstadoEspecialista(idUsuario);

    return sr;
  }
}
