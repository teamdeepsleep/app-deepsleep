import 'package:app_deepsleep/src/controllers/quiz_controller.dart';
import 'package:app_deepsleep/src/utils/formatos.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class QuizScreen extends StatelessWidget {
  const QuizScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(QuizController());
    return GetBuilder<QuizController>(builder: (controller) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Encuesta PSQI'),
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                // mainAxisSize: MainAxisSize.min,
                children: [
                  _tituloPregunta('1'),
                  const Text('¿A qué hora habitualmente te acuestas de noche?'),
                  _viewRespuesta1(controller),
                  TextButton(
                      onPressed: () {
                        _mostartDatapicker(context, controller, 1);
                      },
                      child: const Text('Seleccione Hora')),
                  const Divider(),
                  _tituloPregunta('2'),
                  const Text(
                      '¿Cuantó tiempo normalmente te demoras en quedarte dormido cada noche? Por favor indica tu respuesta en minutos'),
                  _viewRespuesta2(controller),
                  TextButton(
                      onPressed: () {
                        _mostrarInput(context, controller, 2);
                      },
                      child: const Text('Ingresar Datos')),
                  const Divider(),
                  _tituloPregunta('3'),
                  const Text(
                      '¿A qué hora habitualmente te despiertas en las mañanas?'),
                  _viewRespuesta3(controller),
                  TextButton(
                      onPressed: () {
                        _mostartDatapicker(context, controller, 3);
                      },
                      child: const Text('Seleccione Hora')),
                  const Divider(),
                  _tituloPregunta('4'),
                  const Text(
                      '¿Aproximadamente cuántas horas duermes en la noche?'),
                  const Divider(),
                  _viewRespuesta4(controller),
                  TextButton(
                      onPressed: () {
                        _mostrarInput(context, controller, 4);
                      },
                      child: const Text('Seleccione Hora')),
                  _tituloPregunta('5a'),
                  const Text(
                      'En el último mes ha tenido problemas para dormir debido a que ...'),
                  Container(
                      margin: const EdgeInsets.symmetric(vertical: 20.0),
                      child: const Text(
                          'No pudo conciliar el sueño en 30 minutos ')),
                  CheckboxListTile(
                    title: const Text('En ninguna ocasión'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5a1,
                    onChanged: (value) {
                      controller.setRespuesta5a(1, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Menos de una vez a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5a2,
                    onChanged: (value) {
                      controller.setRespuesta5a(2, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Una o dos veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5a3,
                    onChanged: (value) {
                      controller.setRespuesta5a(3, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Tres o más veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5a4,
                    onChanged: (value) {
                      controller.setRespuesta5a(4, value!);
                    },
                  ),
                  const Divider(),
                  _tituloPregunta('5b'),
                  const Text(
                      'En el último mes ha tenido problemas para dormir debido a que ...'),
                  Container(
                      margin: const EdgeInsets.symmetric(vertical: 20.0),
                      child: const Text(
                          'Se despertó en medio de la noche o muy temprano')),
                  CheckboxListTile(
                    title: const Text('En ninguna ocasión'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5b1,
                    onChanged: (value) {
                      controller.setRespuesta5b(1, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Menos de una vez a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5b2,
                    onChanged: (value) {
                      controller.setRespuesta5b(2, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Una o dos veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5b3,
                    onChanged: (value) {
                      controller.setRespuesta5b(3, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Tres o más veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5b4,
                    onChanged: (value) {
                      controller.setRespuesta5b(4, value!);
                    },
                  ),
                  const Divider(),
                  _tituloPregunta('5c'),
                  const Text(
                      'En el último mes ha tenido problemas para dormir debido a que ...'),
                  Container(
                      margin: const EdgeInsets.symmetric(vertical: 20.0),
                      child: const Text('Tuvo que despertar para ir al baño')),
                  CheckboxListTile(
                    title: const Text('En ninguna ocasión'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5c1,
                    onChanged: (value) {
                      controller.setRespuesta5c(1, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Menos de una vez a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5c2,
                    onChanged: (value) {
                      controller.setRespuesta5c(2, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Una o dos veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5c3,
                    onChanged: (value) {
                      controller.setRespuesta5c(3, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Tres o más veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5c4,
                    onChanged: (value) {
                      controller.setRespuesta5c(4, value!);
                    },
                  ),
                  const Divider(),
                  _tituloPregunta('5d'),
                  const Text(
                      'En el último mes ha tenido problemas para dormir debido a que ...'),
                  Container(
                      margin: const EdgeInsets.symmetric(vertical: 20.0),
                      child: const Text('No puede respirar cómodamente')),
                  CheckboxListTile(
                    title: const Text('En ninguna ocasión'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5d1,
                    onChanged: (value) {
                      controller.setRespuesta5d(1, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Menos de una vez a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5d2,
                    onChanged: (value) {
                      controller.setRespuesta5d(2, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Una o dos veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5d3,
                    onChanged: (value) {
                      controller.setRespuesta5d(3, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Tres o más veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5d4,
                    onChanged: (value) {
                      controller.setRespuesta5d(4, value!);
                    },
                  ),
                  const Divider(),
                  _tituloPregunta('5e'),
                  const Text(
                      'En el último mes ha tenido problemas para dormir debido a que ...'),
                  Container(
                      margin: const EdgeInsets.symmetric(vertical: 20.0),
                      child: const Text('Tosió o rónco muy fuerte')),
                  CheckboxListTile(
                    title: const Text('En ninguna ocasión'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5e1,
                    onChanged: (value) {
                      controller.setRespuesta5e(1, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Menos de una vez a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5e2,
                    onChanged: (value) {
                      controller.setRespuesta5e(2, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Una o dos veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5e3,
                    onChanged: (value) {
                      controller.setRespuesta5e(3, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Tres o más veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5e4,
                    onChanged: (value) {
                      controller.setRespuesta5e(4, value!);
                    },
                  ),
                  const Divider(),
                  _tituloPregunta('5f'),
                  const Text(
                      'En el último mes ha tenido problemas para dormir debido a que ...'),
                  Container(
                      margin: const EdgeInsets.symmetric(vertical: 20.0),
                      child: const Text('Sintío mucho frio')),
                  CheckboxListTile(
                    title: const Text('En ninguna ocasión'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5f1,
                    onChanged: (value) {
                      controller.setRespuesta5f(1, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Menos de una vez a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5f2,
                    onChanged: (value) {
                      controller.setRespuesta5f(2, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Una o dos veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5f3,
                    onChanged: (value) {
                      controller.setRespuesta5f(3, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Tres o más veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5f4,
                    onChanged: (value) {
                      controller.setRespuesta5f(4, value!);
                    },
                  ),
                  const Divider(),
                  _tituloPregunta('5g'),
                  const Text(
                      'En el último mes ha tenido problemas para dormir debido a que ...'),
                  Container(
                      margin: const EdgeInsets.symmetric(vertical: 20.0),
                      child: const Text('Sintío mucho calor')),
                  CheckboxListTile(
                    title: const Text('En ninguna ocasión'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5g1,
                    onChanged: (value) {
                      controller.setRespuesta5g(1, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Menos de una vez a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5g2,
                    onChanged: (value) {
                      controller.setRespuesta5g(2, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Una o dos veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5g3,
                    onChanged: (value) {
                      controller.setRespuesta5g(3, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Tres o más veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5g4,
                    onChanged: (value) {
                      controller.setRespuesta5g(4, value!);
                    },
                  ),
                  const Divider(),
                  _tituloPregunta('5h'),
                  const Text(
                      'En el último mes ha tenido problemas para dormir debido a que ...'),
                  Container(
                      margin: const EdgeInsets.symmetric(vertical: 20.0),
                      child: const Text('Tuvo alguna pesadilla')),
                  CheckboxListTile(
                    title: const Text('En ninguna ocasión'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5h1,
                    onChanged: (value) {
                      controller.setRespuesta5h(1, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Menos de una vez a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5h2,
                    onChanged: (value) {
                      controller.setRespuesta5h(2, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Una o dos veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5h3,
                    onChanged: (value) {
                      controller.setRespuesta5h(3, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Tres o más veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5h4,
                    onChanged: (value) {
                      controller.setRespuesta5h(4, value!);
                    },
                  ),
                  const Divider(),
                  _tituloPregunta('5i'),
                  const Text(
                      'En el último mes ha tenido problemas para dormir debido a que ...'),
                  Container(
                      margin: const EdgeInsets.symmetric(vertical: 20.0),
                      child: const Text('Tuvo algún tipo de dolor')),
                  CheckboxListTile(
                    title: const Text('En ninguna ocasión'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5i1,
                    onChanged: (value) {
                      controller.setRespuesta5i(1, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Menos de una vez a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5i2,
                    onChanged: (value) {
                      controller.setRespuesta5i(2, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Una o dos veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5i3,
                    onChanged: (value) {
                      controller.setRespuesta5i(3, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Tres o más veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion5i4,
                    onChanged: (value) {
                      controller.setRespuesta5i(4, value!);
                    },
                  ),
                  const Divider(),
                  _tituloPregunta('6'),
                  const Text(
                      'En el último mes, ¿Cómo calificaría su calidad de sueño en general?'),
                  CheckboxListTile(
                    title: const Text('Muy buena'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion61,
                    onChanged: (value) {
                      controller.setRespuesta6(1, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Relativamente Buena'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion62,
                    onChanged: (value) {
                      controller.setRespuesta6(2, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Relativamente Mala'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion63,
                    onChanged: (value) {
                      controller.setRespuesta6(3, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Muy Mala'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion64,
                    onChanged: (value) {
                      controller.setRespuesta6(4, value!);
                    },
                  ),
                  const Divider(),
                  _tituloPregunta('7'),
                  const Text(
                      'En el último mes, ¿Qué tan seguido ha recorrido a medicamentos que le ayuden a dormir?'),
                  CheckboxListTile(
                    title: const Text('En ninguna ocasión'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion71,
                    onChanged: (value) {
                      controller.setRespuesta7(1, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Menos de una vez a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion72,
                    onChanged: (value) {
                      controller.setRespuesta7(2, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Una o dos veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion73,
                    onChanged: (value) {
                      controller.setRespuesta7(3, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Tres o más veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion74,
                    onChanged: (value) {
                      controller.setRespuesta7(4, value!);
                    },
                  ),
                  const Divider(),
                  _tituloPregunta('8'),
                  const Text(
                      'En el último mes, ¿Con qué frecuencia ha tenido problemas para mantenerse despierto mientras conduce, come o realiza otras actividades?'),
                  CheckboxListTile(
                    title: const Text('En ninguna ocasión'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion81,
                    onChanged: (value) {
                      controller.setRespuesta8(1, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Menos de una vez a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion82,
                    onChanged: (value) {
                      controller.setRespuesta8(2, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Una o dos veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion83,
                    onChanged: (value) {
                      controller.setRespuesta8(3, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Tres o más veces a la semana'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion84,
                    onChanged: (value) {
                      controller.setRespuesta8(4, value!);
                    },
                  ),
                  const Divider(),
                  _tituloPregunta('9'),
                  const Text(
                      'En el último mes, ¿Qué tan problematico ha sido para usted mantener el entusiasmo suficiente para hacer las cosas?'),
                  CheckboxListTile(
                    title: const Text('No hay problema'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion91,
                    onChanged: (value) {
                      controller.setRespuesta9(1, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Solo un problema muy leve'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion92,
                    onChanged: (value) {
                      controller.setRespuesta9(2, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Un poco problemático'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion93,
                    onChanged: (value) {
                      controller.setRespuesta9(3, value!);
                    },
                  ),
                  CheckboxListTile(
                    title: const Text('Un problema muy grande'),
                    controlAffinity: ListTileControlAffinity.leading,
                    value: controller.opcion94,
                    onChanged: (value) {
                      controller.setRespuesta9(4, value!);
                    },
                  ),
                  const Divider(),
                  Container(
                    width: double.infinity,
                    child: ElevatedButton(
                      child: const Text('Enviar Encuesta'),
                      onPressed: () async {
                        _enviarPOSTEncuesta(controller, context);
                      },
                    ),
                  ),
                ],
              ),
            ),
          ));
    });
  }
}

Widget _tituloPregunta(String titulo) {
  return Container(
    width: double.infinity,
    child: Text(
      'Pregunta $titulo:',
      style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
    ),
  );
}

_mostartDatapicker(
    BuildContext context, QuizController controller, int pregunta) async {
  final TimeOfDay? time =
      await showTimePicker(context: context, initialTime: TimeOfDay.now());

  if (time != null) {
    controller.changePregunta(pregunta, time);
  }
}

void _mostrarInput(
    BuildContext context, QuizController controller, int pregunta) {
  showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext _) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadiusDirectional.circular(15)),
          elevation: 5,
          title: const Text('Ingresar'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              (pregunta == 2)
                  ? TextField(
                      controller: controller.respuesta2,
                      keyboardType: TextInputType.number,
                      decoration: const InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        label: Text('Ingrese minutos'),
                      ),
                    )
                  : Container(),
              (pregunta == 4)
                  ? TextField(
                      controller: controller.respuesta4,
                      keyboardType: TextInputType.number,
                      decoration: const InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        label: Text('Ingrese horas'),
                      ),
                    )
                  : Container(),
            ],
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('Cancelar'),
            ),
            TextButton(
              onPressed: () async {
                var respuesta =
                    await controller.changePregunta(pregunta, TimeOfDay.now());
                Navigator.pop(context);
              },
              child: const Text('Ok'),
            )
          ],
        );
      });
}

_enviarPOSTEncuesta(QuizController controller, BuildContext context) async {
  var c = await controller.enviarEncuesta();
  if (c == 1) {
    Navigator.pop(context);
  }
}

Widget _viewRespuesta1(QuizController controller) {
  if (controller.showRespuesta1 == true) {
    print(controller.respuesta1);
    return Text(
        '${textFormatoTiempo00('${controller.respuesta1.hour}')} : ${textFormatoTiempo00('${controller.respuesta1.minute}')}');
  } else {
    return Container();
  }
}

Widget _viewRespuesta2(QuizController controller) {
  if (controller.showRespuesta2 == true) {
    return Text('${textFormatoTiempo00(controller.respuesta2.text)} min');
  } else {
    return Container();
  }
}

Widget _viewRespuesta3(QuizController controller) {
  if (controller.showRespuesta3 == true) {
    return Text(
        '${textFormatoTiempo00('${controller.respuesta3.hour}')} : ${textFormatoTiempo00('${controller.respuesta3.minute}')}');
  } else {
    return Container();
  }
}

Widget _viewRespuesta4(QuizController controller) {
  if (controller.showRespuesta4 == true) {
    return Text('${textFormatoTiempo00(controller.respuesta4.text)} h');
  } else {
    return Container();
  }
}
