// To parse this JSON data, do
//
//     final responseDetalleEspecialista = responseDetalleEspecialistaFromMap(jsonString);

import 'dart:convert';

class ResponseDetalleEspecialista {
  ResponseDetalleEspecialista({
    required this.operacion,
    required this.data,
    required this.mensaje,
  });

  final bool operacion;
  final DetalleEspecialista data;
  final String mensaje;

  factory ResponseDetalleEspecialista.fromJson(String str) =>
      ResponseDetalleEspecialista.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory ResponseDetalleEspecialista.fromMap(Map<String, dynamic> json) =>
      ResponseDetalleEspecialista(
        operacion: json["operacion"],
        data: DetalleEspecialista.fromMap(json["data"]),
        mensaje: json["mensaje"],
      );

  Map<String, dynamic> toMap() => {
        "operacion": operacion,
        "data": data.toMap(),
        "mensaje": mensaje,
      };
}

class DetalleEspecialista {
  DetalleEspecialista({
    required this.id,
    required this.nombre,
    required this.apellido,
    required this.dni,
    required this.correo,
    required this.habilitado,
    required this.v,
    required this.imagen,
    required this.telefono,
    this.trabajo = '',
    required this.especialidades,
  });

  final String id;
  final String nombre;
  final String apellido;
  final String dni;
  final String correo;
  final bool habilitado;
  final int v;
  final String imagen;
  final String telefono;
  late String trabajo;
  late List<String> especialidades;

  factory DetalleEspecialista.fromJson(String str) =>
      DetalleEspecialista.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory DetalleEspecialista.fromMap(Map<String, dynamic> json) =>
      DetalleEspecialista(
        id: json["_id"],
        nombre: json["nombre"],
        apellido: json["apellido"],
        dni: json["dni"],
        correo: json["correo"],
        habilitado: json["habilitado"],
        v: json["__v"],
        imagen: json["imagen"] ??
            'https://www.tenforums.com/geek/gars/images/2/types/thumb_15951118880user.png',
        telefono: json["telefono"] ?? '',
        trabajo: json["trabajo"] ?? '',
        especialidades: json["especialidades"] == null
            ? []
            : List<String>.from(json["especialidades"].map((x) => x)),
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "nombre": nombre,
        "apellido": apellido,
        "dni": dni,
        "correo": correo,
        "habilitado": habilitado,
        "__v": v,
        "imagen": imagen,
        "telefono": telefono,
        "trabajo": trabajo,
        "especialidades": List<dynamic>.from(especialidades.map((x) => x)),
      };
}
