// To parse this JSON data, do
//
//     final especialistaResponse = especialistaResponseFromMap(jsonString);

import 'dart:convert';

class EspecialistaModel {
  EspecialistaModel({
    required this.id,
    required this.nombre,
    required this.apellido,
    required this.dni,
    required this.correo,
    required this.telefono,
    required this.habilitado,
    required this.v,
    this.imagen = '',
  });

  String id;
  String nombre;
  String apellido;
  String dni;
  String correo;
  String telefono;
  bool habilitado;
  int v;
  String imagen;

  factory EspecialistaModel.fromJson(String str) =>
      EspecialistaModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory EspecialistaModel.fromMap(Map<String, dynamic> json) =>
      EspecialistaModel(
        id: json["_id"],
        nombre: json["nombre"],
        apellido: json["apellido"],
        dni: json["dni"],
        telefono: json["telefono"] ?? '',
        correo: json["correo"],
        habilitado: json["habilitado"],
        v: json["__v"],
        imagen: json["imagen"] ??
            'https://www.tenforums.com/geek/gars/images/2/types/thumb_15951118880user.png',
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "nombre": nombre,
        "apellido": apellido,
        "dni": dni,
        "telefono": telefono,
        "correo": correo,
        "habilitado": habilitado,
        "__v": v,
        "imagen": imagen,
      };
}
