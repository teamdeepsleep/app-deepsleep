import 'package:app_deepsleep/src/environment/index.dart';
import 'package:app_deepsleep/src/models/models.dart';
import 'package:app_deepsleep/src/models/response/especialista_response.dart';
import 'package:app_deepsleep/src/models/response/paciente_response.dart';
import 'package:get/get_connect.dart';

class ProfileProvider extends GetConnect {
  static const apiUrl = Environment.baseUrl;
  @override
  void onInit() {}

  Future<PacienteResponse> buscarProfilePaciente(String idPaciente) async {
    var url = '$apiUrl/paciente/$idPaciente';
    PacienteResponse pacientePorId;
    final resp = await get(url);
    if (resp.statusCode == 200) {
      pacientePorId = PacienteResponse.fromMap(resp.body);
    } else {
      pacientePorId = PacienteResponse(
        id: '',
        apellido: '--',
        correo: '---',
        dni: '',
        telefono: '',
        habilitado: false,
        nombre: '',
        psqi: 0,
        recomendaciones: [],
        v: 0,
      );
    }

    return pacientePorId;
  }

  Future<ResponseEspecialista> buscarProfileEspecialista(
      String idPaciente) async {
    var url = '$apiUrl/especialista/$idPaciente';

    final resp = await get(url);
    final data = ResponseEspecialista.fromMap(resp.body);

    return data;
  }

  Future editarPaciente(idPaciente, telefono) async {
    var url = '$apiUrl/paciente/$idPaciente';

    var respuesta = await put(url, {"telefono": telefono});

    return respuesta;
  }
}
