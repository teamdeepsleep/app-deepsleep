import 'package:app_deepsleep/src/models/models.dart';
import 'package:get/get.dart';

class ScheduleController extends GetxController {
  List<Alarm> listAlarms = [];

  agregarAlarma(alarma) {
    listAlarms.add(alarma);
    
    update();
  }

  eliminarAlarma() {}
  activarAlarma(bool value) {}

  cambiarEstadoAlarma(bool state, int index) {
    listAlarms[index].isActived = state;
    update();
  }

}
