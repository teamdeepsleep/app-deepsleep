import 'package:app_deepsleep/src/environment/index.dart';
// import 'package:app_deepsleep/src/models/especialista_model.dart';
import 'package:app_deepsleep/src/models/response/detalle_especialista_response.dart';
import 'package:app_deepsleep/src/models/response/especialista_lista_response.dart';
// import 'package:app_deepsleep/src/models/response/especialista_response.dart';
import 'package:get/get.dart';

class EspecialistaProvider extends GetConnect {
  static const apiUrl = Environment.baseUrl;
  @override
  void onInit() {}

  Future<List<DetalleEspecialista>> listarEspecialistas() async {
    var url = '$apiUrl/especialista/activos';

    final resp = await get(url);

    if (resp.statusCode == 200) {
      return List<DetalleEspecialista>.from(
          resp.body.map((x) => DetalleEspecialista.fromMap(x)));
    } else {
      return [];
    }
  }

  Future miEstadoEspecialista(idPaciente) async {
    var url = '$apiUrl/paciente/miespecialista/$idPaciente';
    final resp = await get(url);

    return resp.body;
  }

  Future<bool> asignarEspecialista(idEspecialista, idPaciente) async {
    var url = '$apiUrl/paciente/miespecialista/$idEspecialista';
    final resp = await post(url, {"id": idPaciente});
    if (resp.statusCode == 200) {
      Get.snackbar(
          'asignar', 'Se envio su solicitud, porfavor esperar la confirmación');
      return true;
    } else {
      return false;
    }
  }

  Future listarMisPotencialesPacientes(idEspecialista) async {
    var url = '$apiUrl/especialista/misposiblespacientes/$idEspecialista';
    final resp = await get(url);

    if (resp.statusCode == 200) {
      ResponseEspecialistaLista especialistas =
          ResponseEspecialistaLista.fromMap(resp.body);

      //   return List<EspecialistaResponse>.from(
      //       resp.body.map((x) => EspecialistaResponse2.fromMap(x)));

      return especialistas.data;
    } else {
      return [];
    }
  }

  Future listarMisPacientes(idEspecialista) async {
    var url = '$apiUrl/especialista/mispacientes/$idEspecialista';
    final resp = await get(url);

    if (resp.statusCode == 200) {
      ResponseEspecialistaLista especialistas =
          ResponseEspecialistaLista.fromMap(resp.body);

      //   return List<EspecialistaResponse>.from(
      //       resp.body.map((x) => EspecialistaResponse2.fromMap(x)));

      return especialistas.data;
    } else {
      return [];
    }
  }

  Future aceptarInvitacionDe(idPaciente, idEspecialista) async {
    var url =
        '$apiUrl/especialista/$idEspecialista/miposiblepaciente/$idPaciente/aceptar';

    final resp = await put(url, {});
    print(resp.body);
    return resp.body["operacion"];
  }

  Future<ResponseDetalleEspecialista> datosDetalleEspecialista(
      String idEspecialista) async {
    var url = '$apiUrl/especialista/detalle/$idEspecialista';

    final resp = await get(url);
    var datos = ResponseDetalleEspecialista.fromMap(resp.body);
    // print(resp.body);
    return datos;
  }

  Future<bool> agregarEspecialidad(
      String idEspecialista, String textEspecialidad) async {
    var url = '$apiUrl/especialista/$idEspecialista/agregarEspecialidad';

    final resp = await put(url, {"textEspecialidad": textEspecialidad});
    return resp.body["operacion"];
  }

  Future<bool> eliminarEspecialidad(String idEspecialista, int index) async {
    var url = '$apiUrl/especialista/$idEspecialista/eliminarEspecialidad';

    final resp = await put(url, {"index": index});
    return resp.body["operacion"];
  }

  Future<bool> actualizarEspecialista(
      String idEspecialista, String campo, String valor) async {
    var url = '$apiUrl/especialista/$idEspecialista/actualizarEspecialista';

    final resp = await put(url, {"campo": campo, "valor": valor});

    return resp.body["operacion"];
  }
}
