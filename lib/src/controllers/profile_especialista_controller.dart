// import 'package:app_deepsleep/src/models/especialista_model.dart';
import 'package:app_deepsleep/src/providers/especialista_provider.dart';
// import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../models/response/detalle_especialista_response.dart';

class ProfileEspecialistaController extends GetxController {
  final especialistaProvider = Get.find<EspecialistaProvider>();

  ProfileEspecialistaController() {}

//   Future buscarProfileEspecialista() async {}

  Future<List<DetalleEspecialista>> listarEspecialistas() async {
    return especialistaProvider.listarEspecialistas();
  }

  Future asignarEspecialista(idEspecialista) async {
    var storage = GetStorage();
    var idUsuario = await storage.read('idUsuario');
    especialistaProvider.asignarEspecialista(idEspecialista, idUsuario);
  }
}
