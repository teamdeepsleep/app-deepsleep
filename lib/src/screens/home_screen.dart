import 'package:app_deepsleep/src/controllers/bottom_navigation_controller.dart';
import 'package:app_deepsleep/src/controllers/recommendation_controller.dart';
import 'package:app_deepsleep/src/screens/screens.dart';
import 'package:app_deepsleep/src/themes/app_themes.dart';
import 'package:app_deepsleep/src/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomePacienteScreen extends StatelessWidget {
  const HomePacienteScreen({Key? key}) : super(key: key);
  static List<StatelessWidget> tab = const [
    RecommendationScreen(),
    MessagePacienteScreen(),
    ScheduleScreen(),
    MusicScreen(),
    DashboardPacienteScreen()
  ];

  @override
  Widget build(BuildContext context) {
    Get.put(BottomNavigationController());

    return GetBuilder<BottomNavigationController>(builder: (controller) {
      return Scaffold(
        appBar: AppBar(
          title: Text(controller.tituloAppBar),
          actions: [
            (controller.selectIndex == 0)
                ? Container(
                    padding: EdgeInsets.only(right: 10),
                    child: InkWell(
                      onTap: () async {
                        var f = Get.put(RecommendationController());
                        f.listarMisRecomendaciones();
                      },
                      child: CircleAvatar(
                        child: Icon(Icons.refresh),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
        drawer: _crearDrawerPaciente(context, controller),
        body: tab[controller.selectIndex],
        bottomNavigationBar: CustomBottomNavigationPaciente(
          controller: controller,
        ),
      );
    });
  }

  Drawer _crearDrawerPaciente(
      BuildContext context, BottomNavigationController controller) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Image.asset(
                  'assets/img/reading.png',
                  fit: BoxFit.cover,
                  height: 80,
                  width: 80,
                ),
                const Text('Deep Sleep Menu')
              ],
            ),
            decoration: const BoxDecoration(color: AppTheme.primary),
          ),
          _item(Icons.perm_identity, 'Mi Perfil', 'profile_paciente', context),
          _item(Icons.support, 'Encuesta', 'quiz', context),
          _item(Icons.group, 'Especialistas', 'specialists', context),
          _item(Icons.checklist_outlined, 'Hábitos', 'habit', context),
          _item(Icons.military_tech_sharp, 'Meta', 'goal', context),
          const Divider(),
          ListTile(
            leading: const Icon(Icons.logout, color: AppTheme.primary),
            title: const Text('Cerrar Sesión'),
            onTap: () {
              //   controller.logout();
              //   Navigator.pushReplacementNamed(context, 'login');
              displayDialogAndroidCerrarSesionPaciente(context, controller);
            },
          )
        ],
      ),
    );
  }
}

Widget _item(
    IconData icon, String titulo, String router, BuildContext context) {
  return ListTile(
    leading: Icon(icon, color: AppTheme.primary),
    title: Text(titulo),
    onTap: () {
      Navigator.pop(context);
      Navigator.pushNamed(context, router);
    },
  );
}

displayDialogAndroidCerrarSesionPaciente(
    BuildContext context, BottomNavigationController controller) {
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadiusDirectional.circular(15)),
        elevation: 5,
        title: const Text('Cerrar Sesion'),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: const [
            Text('¿Desea cerrar sesión?'),
          ],
        ),
        actions: [
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: const Text('Cancelar'),
          ),
          TextButton(
            onPressed: () async {
              controller.logout();
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, 'login');
            },
            child: const Text('SI'),
          )
        ],
      );
    },
  );
}
