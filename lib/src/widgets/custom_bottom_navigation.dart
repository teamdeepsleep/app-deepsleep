import 'package:app_deepsleep/src/controllers/bottom_navigation_controller.dart';
import 'package:flutter/material.dart';

class CustomBottomNavigationPaciente extends StatelessWidget {
  final BottomNavigationController controller;
  const CustomBottomNavigationPaciente({Key? key, required this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // UiBloc uiBloc = BlocProvider.of<UiBloc>(context);
    return BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: controller.selectIndex,
        onTap: (i) {
          controller.changeIndex(i);
        },
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.recommend_rounded),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.message_rounded),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.alarm_rounded),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.my_library_music_rounded),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.dashboard_rounded),
            label: '',
          ),
          // BottomNavigationBarItem(
          //   icon: Icon(Icons.star_outlined),
          //   label: '',
          // ),
        ]);
  }
}
