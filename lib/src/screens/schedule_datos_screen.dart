import 'package:app_deepsleep/src/controllers/schedule_datos_controller.dart';
import 'package:app_deepsleep/src/models/alarm_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ScheduleDatosScreen extends StatelessWidget {
  const ScheduleDatosScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(ScheduleDatosController());

    return GetBuilder<ScheduleDatosController>(builder: (controller) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Datos Alarma'),
        ),
        body: SafeArea(
          child: Container(
            // color: Colors.red,
            width: double.infinity,
            child: Column(
              children: [
                const SizedBox(height: 20),
                Card(
                  child: InkWell(
                    onTap: () async {
                      _mostrarDatapicker(context, controller, 1);
                    },
                    child: ListTile(
                      trailing: const Icon(Icons.alarm_add),
                      title: Text('${controller.textHoraInicio.text} '),
                      subtitle: const Text('Inicio'),
                    ),
                  ),
                ),
                Card(
                  child: InkWell(
                    onTap: () async {
                      _mostrarDatapicker(context, controller, 2);
                    },
                    child: ListTile(
                      trailing: const Icon(Icons.alarm_add),
                      title: Text('${controller.textHoraFin.text} '),
                      subtitle: const Text('fin'),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                const Text('Dias'),
                const SizedBox(height: 20),
                Container(
                  //   color: Colors.green,
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      _circleDay('L', controller.isLunes, controller),
                      _circleDay('Ma', controller.isMartes, controller),
                      _circleDay('Mi', controller.isMiercoles, controller),
                      _circleDay('J', controller.isJueves, controller),
                      _circleDay('V', controller.isViernes, controller),
                      _circleDay('S', controller.isSabado, controller),
                      _circleDay('D', controller.isDomingo, controller),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            var isValidated = controller.validarDatos();

            if (isValidated == true) {
              Alarm data = controller.enviarData();
              controller.limpiarData();
              Navigator.pop(context, data);
            }
          },
          child: const Icon(Icons.save),
        ),
      );
    });
  }

  Widget _circleDay(
      String day, bool selectDay, ScheduleDatosController controller) {
    return InkWell(
      onTap: () async {
        // selectDay = !selectDay;
        controller.updateDays(selectDay, day);
      },
      child: CircleAvatar(
        backgroundColor: (selectDay == true) ? Colors.amber : Colors.red,
        child: Text(day),
      ),
    );
  }

  _mostrarDatapicker(BuildContext context, ScheduleDatosController controller,
      int pregunta) async {
    final TimeOfDay? time =
        await showTimePicker(context: context, initialTime: TimeOfDay.now());
    print('dta is $time');
    if (time != null) {
      //setear data
      controller.updateTimes(time, pregunta);
    }
  }
}
