import 'package:app_deepsleep/src/controllers/profile_paciente_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

// https://i.pinimg.com/originals/10/f3/3a/10f33a90120d9cfe137ce22bfdb8d243.jpg
class ProfilePacienteScreen extends StatelessWidget {
  const ProfilePacienteScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var f = Get.put(ProfilePacienteController());
    f.buscarProfilePaciente();
    return GetBuilder<ProfilePacienteController>(builder: (controller) {
      //   var x = controller.buscarProfilePaciente();
      //   controller.buscarProfilePaciente();
      return Scaffold(
        appBar: AppBar(
          title: const Text('Perfil'),
        ),
        body: SafeArea(
            child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image:
                            NetworkImage(controller.pacienteEncontrado.imagen),
                        fit: BoxFit.cover)),
                child: SizedBox(
                  width: double.infinity,
                  height: 200,
                  child: Container(
                    alignment: const Alignment(0.0, 2.5),
                    child: CircleAvatar(
                      backgroundImage:
                          NetworkImage(controller.pacienteEncontrado.imagen),
                      radius: 60.0,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 60,
              ),
              Text(
                '${controller.pacienteEncontrado.nombre} ${controller.pacienteEncontrado.apellidos} ',
                style: const TextStyle(
                    fontSize: 25.0,
                    color: Colors.blueGrey,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.w400),
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                'Celular',
                style: TextStyle(
                    fontSize: 18.0,
                    color: Colors.black45,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.w300),
              ),
              Text(
                controller.pacienteEncontrado.telefono,
                style: const TextStyle(
                    fontSize: 18.0,
                    color: Colors.black45,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                'DNI',
                style: TextStyle(
                    fontSize: 18.0,
                    color: Colors.black45,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.w300),
              ),
              Text(
                controller.pacienteEncontrado.dni,
                style: const TextStyle(
                    fontSize: 18.0,
                    color: Colors.black45,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                'Correo',
                style: TextStyle(
                    fontSize: 18.0,
                    color: Colors.black45,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.w300),
              ),
              Text(
                controller.pacienteEncontrado.correo,
                style: const TextStyle(
                    fontSize: 15.0,
                    color: Colors.black45,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 180,
              ),
              Container(
                width: double.infinity,
                margin: const EdgeInsets.all(8),
                child: ElevatedButton(
                  onPressed: () async {
                    //   Navigator.pushNamed(context, 'profile_edit');
                    displayDialogAndroid(context, controller);
                  },
                  child: const Text('EDITAR'),
                ),
              )
            ],
          ),
        )),
      );
    });
  }
}

void displayDialogAndroid(
    BuildContext context, ProfilePacienteController controller) {
  showDialog(
      barrierDismissible: false,
      context: context,
      builder: (_) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadiusDirectional.circular(15)),
          elevation: 5,
          title: const Text('Editar'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              //   const Text('Ingrese un nuevo teléfono'),
              const SizedBox(height: 15),
              TextFormField(
                keyboardType: TextInputType.number,
                controller: controller.telefono,
                decoration: const InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    border: OutlineInputBorder(),
                    hintText: 'Ingrese un telefono'),
              ),
            ],
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('Cancelar'),
            ),
            TextButton(
              onPressed: () async {
                controller.editarPaciente();
                Navigator.pop(context);
              },
              child: const Text('Ok'),
            )
          ],
        );
      });
}
