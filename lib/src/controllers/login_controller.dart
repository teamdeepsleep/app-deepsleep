import 'package:flutter/material.dart';
import 'package:app_deepsleep/src/providers/providers.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LoginController extends GetxController {
  final loginProvider = Get.find<LoginProvider>();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  bool isLogin = false;

  LoginController() {}
  Future<String> login() async {
    if (email.value.text.isEmpty || password.value.text.isEmpty) {
      Get.snackbar('login', 'Porfavor revisar y completar todos los campos');
      return '';
    }
    cargando(true);
    var login =
        await loginProvider.validarLogin(email.value.text, password.value.text);
    if (login.operacion == true) {
      var storage = GetStorage();
      if (login.tipo == 'paciente') {
        var token = login.token;
        var usuarioTipo = 'usuario';
        var idUsuario = login.id;

        storage.write('isLogued', true);
        storage.write('token', token);
        storage.write('usuarioTipo', usuarioTipo);
        storage.write('idUsuario', idUsuario);

        cargando(false);
        return 'usuario';
      }
      if (login.tipo == 'especialista') {
        var token = login.token;
        var usuarioTipo = 'especialista';
        var idUsuario = login.id;

        storage.write('isLogued', true);
        storage.write('token', token);
        storage.write('usuarioTipo', usuarioTipo);
        storage.write('idUsuario', idUsuario);

        cargando(false);
        return 'especialista';
      } else {
        return '';
      }
    } else {
      cargando(false);
      Get.snackbar('login', login.mensaje);
      return '';
    }
  }

  cargando(bool value) {
    isLogin = value;
    update();
  }
}
