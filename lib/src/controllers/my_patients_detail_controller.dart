// import 'package:app_deepsleep/src/models/models.dart';
import 'package:app_deepsleep/src/providers/especialista_provider.dart';
import 'package:app_deepsleep/src/providers/profile_provider.dart';
import 'package:app_deepsleep/src/models/response/especialista_lista_response.dart';
import 'package:app_deepsleep/src/models/response/paciente_response.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class MyPatientsDetailController extends GetxController {
  final profileProvider = Get.find<ProfileProvider>();
  final especialistaProvider = Get.find<EspecialistaProvider>();
//   Paciente pacienteEncontrado = Paciente();
  MyPatientsDetailController();

  Future<PacienteResponse> buscarDetailPaciente(String idPaciente) async {
    PacienteResponse pacientePorId =
        await profileProvider.buscarProfilePaciente(idPaciente);

    return pacientePorId;
  }

  Future<bool> aceptarInvitacionPorId(String idPaciente) async {
    var storage = GetStorage();
    var idUsuario = await storage.read('idUsuario');
    return await especialistaProvider.aceptarInvitacionDe(
        idPaciente, idUsuario);
  }
}
